const EthSN = require('./index')
/** interprets params (same as in geth)
Node configuration
  --datadir "/Users/guenole/Library/Ethereum"  Data directory for the databases and keystore
  --keystore value                             Directory for the keystore (default = inside the datadir)
RPC Service
  --rpc                  Enable the HTTP-RPC server
  --rpcaddr value        HTTP-RPC server listening interface (default: "localhost")
  --rpcport value        HTTP-RPC server listening port (default: 8545)
  --rpcapi value         API's offered over the HTTP-RPC interface
  --rpccorsdomain value  Comma separated list of domains from which to accept cross origin requests (browser enforced)
  --ws                   Enable the WS-RPC server
  --wsaddr value         WS-RPC server listening interface (default: "localhost")
  --wsport value         WS-RPC server listening port (default: 8546)
  --wsapi value          API's offered over the WS-RPC interface
  --wsorigins value      Origins from which to accept websockets requests
*/

const commandLineArgs = require('command-line-args')
const commandLineUsage = require('command-line-usage')

const optionDefinitions = [
    { name: 'datadir', type: String, defaultValue:'datadir', description: 'Data directory for the databases and keystore'},
    { name: 'keystore', type: String, defaultValue: '{datadir}/keystore', description:'Directory for the keystore (default = inside the datadir)' },
    { name: 'networkid', alias: 'i', type: Number, defaultValue:6000, description:'Network identifier (integer, could use official numbers, default 6000)'},
    { name: 'rpcport', type: Number, defaultValue: 8545, description:'HTTP-RPC server listening port (default: 8545)'},
    { name: 'rpcapi', type: String, defaultValue: 'eth,net,personal', description:'API\'s offered over the HTTP-RPC interface' },
    {
        name: 'help',
        alias: 'h',
        type: Boolean,
        description: 'Display this usage guide.'
      }
  ]
const options = commandLineArgs(optionDefinitions) 
options.keystore = options.keystore.replace('{datadir}', options.datadir)

if (options.help) {
  const usage = commandLineUsage([
    {
      header: 'Ethereum Standalone Node',
      content: 'A nodejs implementation of a Ethereum node that does not talk to other node but handle a EVM and accept Web3 json-rpc interaction.'
    },
    {
      header: 'Options',
      optionList: optionDefinitions
    },
    {
      content: 'Project home: {underline https://gitlab.com/guenoledc-perso/offchainprivatetx}'
    }
  ])
  console.log(usage)
  process.exit(1)
} else {
  //console.log(options)
}

// INSTALL PROCESS HANDLING
process.on('uncaughtException', (err) => {
    console.log('Caught exception:', err);
});
function gracefullStop() {
    // do some stuff on the servers
    if(esn) esn.graceFullStop(err=>{
      if(err) console.error('Failure when freeing up resources', err)
      process.exit(0)
    })
}
process.on('SIGTERM', ()=>{
    console.log('Stopping process gracefully requested via SIGTERM')
    gracefullStop()
})
process.on('SIGINT', ()=>{
    console.log('Stopping process gracefully requested via SIGINT')    
    gracefullStop()
})

const mkdirp = require('mkdirp');
mkdirp.sync(options.datadir+'/data', 0777)
mkdirp.sync(options.keystore, 0777)

let esn = new EthSN({dbPath:options.datadir+'/data', keystorePath:options.keystore, networkId:options.networkid});

esn.initialization( () => {
    let infos = esn.getBlockchainInfo()
    console.log('Initialized ', infos.chainName, 'version:', infos.version, 'protocol version:', infos.protocolVersion, 'currentBlock:', infos.blockNumber.toString())
    console.log('   database dir:', esn.options.dbPath)
    console.log('   keystore dir:', esn.options.keystorePath)

    HttpHandler(esn, options)
})


function HttpHandler(esn, options) {
  let rpcHandler = esn.jsonRpcHandler()
  rpcHandler.trace=true;
  let http=require('http')

  function addAccessControlHeaders(request, response) {
    if(!request.headers.origin) return // no origin, test also here if CORS is allowed
    response.setHeader('Access-Control-Allow-Origin', request.headers.origin);
    response.setHeader('Vary', 'Origin');
  }
  function onOptions(request, response) {
    addAccessControlHeaders(request, response)
    response.writeHead(200, {
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'POST',
      'Access-Control-Max-Age': 600,
      'Vary': 'Access-Control-Request-Method',
      'Vary': 'Access-Control-Request-Headers',
      "Content-Type" : "text/plain; charset=utf-8"
    })
    response.end('')
  }
  let server=http.createServer( 
    function(request,response) {
      //console.log('request headers', request.headers)
      //console.log('request.method', request.method)
      if(request.method=='OPTIONS') return onOptions(request, response)
      if(request.method=='GET') { response.writeHead(200, {"Content-Type" : "application/json"}); response.end('{status:"ok"}\n');}
      if(request.method!='POST') { response.writeHead(404, {}); response.end(''); return}
      // TODO : Fail if wrong method, wrong content-type, wrong data size
      let data = new Buffer(0)
      request.on('data', chunk => {
        //console.log(data, chunk)
        data = Buffer.concat([data,chunk])
      });
      request.on('end', () => {
        //console.log('Request is ', data.toString())
        addAccessControlHeaders(request, response)
        rpcHandler.submitAsync(data.toString('utf8'), (jsonResponse, status)=> {
          response.writeHead(status, {"Content-Type" : "application/json"});
          //console.log(jsonResponse)
          response.end(jsonResponse+'\n');  
        })
      });
  });
  server.listen(options.rpcport);
  server.on('listening', ()=>console.log('HTTP-RPC service started on port', options.rpcport))
}