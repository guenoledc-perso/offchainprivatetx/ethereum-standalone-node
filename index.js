const async = require('async');
const level = require('level');
const levelMem = require('level-mem');
const Stoplight = require('flow-stoplight')
const AsyncEventEmitter = require('async-eventemitter');

const Trie = require('merkle-patricia-tree/secure');
const Transaction = require('ethereumjs-tx');
const EthUtils = require('ethereumjs-util');
const BN = EthUtils.BN;
EthUtils.isValidHash256 = function (hash) {
    return (/^0x[0-9a-fA-F]{64}$/.test(hash)
    );
  };
  

const BlockchainWrapper = require('./lib/blockchain-wrapper');
const EthVM = require('./lib/ethereum-vm');
const TransactionReceipts = require('./lib/transaction-receipts');
const JSONRPCHandler = require('./lib/rpc/app-rpc-handler');
const Web3Provider = require('./lib/rpc/web3-provider')
const Keystore = require('./lib/keystore')
const PendingTransactions = require('./lib/pendingTransactions')
const ContextDataProvider = require('./lib/context-data-provider')

const default_options = {keystorePath:'./.keystore', dbPath:'./.ethdb', networkId:6000, extra_data:'0x0'};
const pendingTransactionsKey = 'fr.yahoo.guenoledc.pendingTx';

function formatBlock(block, withFullTransactions) {
    // return a block that follow specification at https://github.com/ethereum/wiki/wiki/JSON-RPC#returns-26
    let b = {};
    b.number = '0x'+new BN(block.header.number).toString(16);
    b.hash = EthUtils.bufferToHex(block.hash());
    b.parentHash = EthUtils.bufferToHex(block.header.parentHash);
    b.nonce = EthUtils.bufferToHex((block.header.nonce));
    b.sha3Uncles = EthUtils.bufferToHex(block.header.uncleHash);
    b.logsBloom = EthUtils.bufferToHex(block.header.bloom);
    b.transactionsRoot = EthUtils.bufferToHex(block.header.transactionsTrie);
    b.stateRoot = EthUtils.bufferToHex(block.header.stateRoot);
    b.receiptsRoot = EthUtils.bufferToHex(block.header.receiptTrie);
    b.miner = EthUtils.bufferToHex(block.header.coinbase);
    b.difficulty = EthUtils.bufferToHex(block.header.difficulty);
    b.totalDifficulty = EthUtils.bufferToHex(block.header.difficulty);
    b.size = EthUtils.addHexPrefix(block.serialize().length.toString(16));
    b.gasLimit = EthUtils.bufferToHex(block.header.gasLimit);
    b.gasUsed = '0x'+new BN(block.header.gasUsed).toString(16);
    b.extraData = EthUtils.bufferToHex(block.header.extraData);
    b.timestamp = EthUtils.bufferToHex(block.header.timestamp);
    if(withFullTransactions)
        b.transactions = block.transactions.map( (tx,index)=>formatTransaction(block,index));
    else b.transactions = block.transactions.map( tx=> EthUtils.bufferToHex(tx.hash()) );
    b.uncles = block.uncleHeaders.map( u=>EthUtils.bufferToHex(u.hash()))
    return b;
}
const formatPendingTransaction = PendingTransactions.formatPendingTransaction

function formatTransaction(block, index) {
    // create a response respecting the format in https://github.com/ethereum/wiki/wiki/JSON-RPC#returns-28
    let tx = block.transactions[index];
    let transaction = {
        blockHash: EthUtils.bufferToHex(block.hash()),
        blockNumber: '0x'+new BN(block.header.number).toString(16),
        from: EthUtils.bufferToHex(tx.from),
        gas: EthUtils.bufferToHex(tx.gas),
        gasPrice: '0x'+new BN(tx.gasPrice).toString(16),
        hash: EthUtils.bufferToHex(tx.hash()),
        input: EthUtils.bufferToHex(tx.input),
        nonce: '0x'+new BN(tx.nonce).toString(16),
        to: '0x'+new BN(tx.to).toString(16),
        transactionIndex: EthUtils.addHexPrefix(index.toString(16)),
        value: '0x'+new BN(tx.value).toString(16),
        v: '0x'+new BN(tx.v).toString(16),
        r: EthUtils.bufferToHex(tx.r),
        s: EthUtils.bufferToHex(tx.s)
    };

    return transaction;
}

function formatPlayTransactionReceipt(result) {
    // create a response respecting the format in https://github.com/ethereum/wiki/wiki/JSON-RPC#returns-31
    let receipt = {
        transactionHash: EthUtils.bufferToHex(result.transaction.hash()),
        transactionIndex: null,
        blockHash: null,
        blockNumber: null,
        from: EthUtils.bufferToHex(result.transaction.from),
        to: EthUtils.bufferToHex(result.transaction.to),
        gasUsed: EthUtils.bufferToHex(result.gasUsed), 
        cumulativeGasUsed: EthUtils.bufferToHex(result.gasUsed), 
        contractAddress: null,
        logs: result.vm.logs,
        logsBloom: EthUtils.bufferToHex(result.bloom.bitvector),
        status: result.vm.exception ? 1 : 0,
        return: EthUtils.bufferToHex(result.vm.return),
        exception: result.vm.exception ? null : result.vm.exceptionError.error,
    };

    return receipt;
}

class EthereumStandaloneNode extends AsyncEventEmitter {
    constructor(options) {
        super()
        this.setMaxListeners(30)
        this.options = options;
        if(!this.options)  this.options = default_options;

        if(this.options.dbPath === 'memory')
            this.db = levelMem();
        else 
            this.db = new level(this.options.dbPath || default_options.dbPath);
        if(!this.options.networkId) this.options.networkId = default_options.networkId;

        if(this.options.keystorePath === 'memory')
            this.keystore = new Keystore(null, null);
        else
            this.keystore = new Keystore(this.options.keystorePath || default_options.keystorePath, 1000);

        // NOTE Ordering: this to allow that not all private transactions for a given account are received by this node (breaks the nonce sequence)
        this.vmAcceptsNonceGaps = !!this.options.inPrivacyNode

        if(this.options.context) 
            this.contextDataProvider = this.options.context
        else this.contextDataProvider = new ContextDataProvider()

        // must be created at time of initialization
        this.blockchain = null;
        this.vm = null;
        this.miningActivated = true; // by default receiving new transaction will trigger the block creation, can be changed via miner_start, miner_stop premises
        
        // transactions awaiting to be processed by the VM
        this._pendingTransactions = null;
        this._initialized = false
    }

    jsonRpcHandler(trace) {
        return new JSONRPCHandler(this, null, trace);
    }
    web3Provider(trace) {
        return new Web3Provider(this.jsonRpcHandler(trace))
    }

    initializationWithTestData(testData, cbCompletion) {
        if(this._initialized) throw new Error('Should not initialize the instance twice')
        const init = require('./test/bcInit');
        let self = this;

        async.seq(
            // 1. initialize db and state with the data
            cb => {
                init(testData, self.db, cb);
            },
            // 2. Create the Blockchain and awaits full loading of the blockchain heads from the db
            (result,cb) => {
                BlockchainWrapper.newBlockchain(
                    self.options.networkId || default_options.networkId, 
                    self.options.extra_data || default_options.extra_data, 
                    self.contextDataProvider,
                    self.db, (err, newBlockchain)=>{
                        self.blockchain = newBlockchain
                        cb(err,result)
                    });
            },
            // 3. initialize the VM and set the state Root after the testData have been loaded
            (result, cb) => {
                self.vm = new EthVM(self.db, self.blockchain.vmOpts());
                if(self.vmAcceptsNonceGaps)
                    self.vm.on('beforeTx', self._forceAccountNonceBeforeProcessingTx.bind(self) )
                self._pendingTransactions = new PendingTransactions(self.vm.stateManager, self.vmAcceptsNonceGaps)
                self.vm.setStateRoot(result.root, cb)
            })( (error) => {
                console.log('Initialization completed');
                // can be called one for each error
                if(error) console.error("Initialization failure:", error);
                if(!error) self._initialized=true     
                cbCompletion(error);
            });
    }

    initialization(cbCompletion) {
        if(this._initialized) throw new Error('Should not initialize the instance twice')
        let self = this;
        async.series([
            // 1. Create the Blockchain and awaits full loading of the blockchain heads from the db
            cb => {
                BlockchainWrapper.newBlockchain(
                    self.options.networkId || default_options.networkId, 
                    self.options.extra_data || default_options.extra_data, 
                    self.contextDataProvider,
                    self.db, (err, newBlockchain)=>{
                        self.blockchain = newBlockchain
                        cb(err)
                    });
            },
            // 2. initialize the VM and set its initial state root if necessary
            cb => {
                self.vm = new EthVM(self.db, self.blockchain.vmOpts());
                if(self.vmAcceptsNonceGaps)
                    self.vm.on('beforeTx', self._forceAccountNonceBeforeProcessingTx.bind(self) )
                self._pendingTransactions = new PendingTransactions(self.vm.stateManager, self.vmAcceptsNonceGaps)
                self.vm.initState(null, cb);
            },
            // 3. Load any pending transactions
            cb => {
                self.db.get(pendingTransactionsKey, (err, pendings)=>{
                    if(err)
                        if( err.notFound ) cb()
                        else cb(err)
                    else { // pendings is a rlp encoded from PendingTransaction.serialize()
                        //console.log('Init Data', pendings)
                        self._pendingTransactions.initFromData(EthUtils.toBuffer(pendings), (err, results)=>{
                            self.triggerBlockCreation() // in case there are pending transactions being loaded and if the mining is activated it should process them
                            cb()
                        })
                    }
                })
            },
        ], (error, results) => {
            // can be called one for each error
            if(error) console.error("Initialization failure:", error);
            if(!error) self._initialized=true  
            // results is [undefined, undefined] at the final call but still it is a valid condition
            if(results) cbCompletion(error);
        });
    }
    
    // NOTE Ordering: This is a before tx hook handler to apply when we want to ignore the gap of nonce
    // NOTE Ordering: Apply if applicable the management of gaps in the nonce sequence to accept that a 
    //      tx is sent with a nonce after the current nonce of the account
    _forceAccountNonceBeforeProcessingTx(tx, cbFinal) {
        //console.log('BEFORE TX DURING VM RUNTX', tx.hash().toString('hex'))
        /** Logic: if this.vmAcceptsNonceGaps only
         *      - get the tx from account 
         *      - get the current nonce from the blockchain
         *      - if tx nonce> current nonce; set the current nonce with the tx nonce; else do nothing, the tx will be rejected further down the process
         *  In all case, call the callback with (error)
         */
        if(!tx || !tx.from || !tx.nonce) cb('Provide a valid tx to handle')

        // the vm is not suppose to handle this special feature so exit
        if(!this.vmAcceptsNonceGaps) return cb(null)

        const self = this
        async.seq(
            cb=>cb(null, tx.from), // address in buffer format
            (from, cb)=>self.vm.stateManager.getAccount(from, cb), // cb(err, account)
            (account, cb)=>{
                let ac_nonce = new BN(account.nonce)
                let tx_nonce = new BN(tx.nonce)
                if(tx_nonce.gt(ac_nonce)) {
                    account.nonce = tx.nonce
                    cb(null, account)
                } else cb(null, null) // do not give an account to tell there is nothing to modify
            },
            (account, cb)=>{
                if(!account) return cb(null) // we are done, nothing to do
                // else update the account with the tx nonce
                self.vm.stateManager.putAccount(tx.from, account, cb)
            }
        )(err=>{
            //console.log('BEFORE TX HANDLE DONE')
            cbFinal(err)
        })
    }

    putDb(prefix, name, value, cb) {
        if(!prefix) prefix='root'
        if(!name) throw new TypeError('provide a valid string name to write the db')
        if(!value) value = '0x'
        if(Buffer.isBuffer(value)) value = EthUtils.bufferToHex(value)
        if(typeof value !== 'string') throw new TypeError('expects a Buffer or string value')

        this.db.put(prefix+'.'+name, value, cb)
    } 
    getDb(prefix, name, cb) {
        if(!prefix) prefix='root'
        if(!name) throw new TypeError('provide a valid string name to read the db')
        this.db.get(prefix+'.'+name, (err, value)=>{
            if(err) 
                if( err.notFound ) cb(null, null)
                else cb(err)
            else cb(null, value)
        })
    }

    set coinbase(address) {
        // create a pseudo wallet that cannot return much
        this._coinbaseWallet = { getAddressString: ()=>EthUtils.bufferToHex(address) };
    }
    get coinbase() {
        if(!this._coinbaseWallet) {
            // lets first see if I can get the first account from the keystore
            const accounts=this.keystore.listAccounts()
            if(accounts.length>0) 
                this._coinbaseWallet = { getAddressString: ()=>accounts[0] }
            else { // Lets create a new coinbase but this is dangerous as the private key is lost here
                const Wallet = require('ethereumjs-wallet');
                this._coinbaseWallet = Wallet.generate();
            }
        }
        return this._coinbaseWallet.getAddressString();
    }

    getBlockchainInfo() {
        let opts = this.blockchain.txOpts();
        return {
            version: opts.chainId,
            chainName : opts.chainName,
            listening: false,
            peerCount: 0,
            protocolVersion: 64,
            syncing: false,
            gasPrice: opts.gasPrice,
            blockNumber: new BN(this.blockchain.lastBlockNumber),
            coinbase: this.coinbase
        };
    }

    getAccount(address, withCode, blockNumber, cbFinal) {
        address=EthUtils.addHexPrefix(address);
        if(!EthUtils.isValidAddress(address)) throw TypeError('expecting a valid Ethereum address in string format');
        let addressB = EthUtils.toBuffer(address);
        if( typeof withCode !== 'boolean' ) {blockNumber=withCode; cbFinal=blockNumber; withCode=false;}
        if(!blockNumber) blockNumber='latest';
        if( typeof cbFinal !== 'function' ) throw new TypeError('Expecting a callback function(error, account)');

        let self = this;
        async.seq(
            cb=>{
                self._getVMCopyAtBlock(blockNumber, cb);
            },
            (vm, cb)=>{
                vm.stateManager.getAccount(addressB, (err, account) => {
                    if( err ) return cb(err, null);
                    let res = {
                        address: address,
                        balance: new BN(account.balance),
                        nonce: new BN(account.nonce),
                        isContract: account.isContract(),
                        code: '0x'
                    }
                    if( res.isContract && withCode === true ) 
                        account.getCode(vm.stateManager._trie, (e, c) => {
                            if(e) return cb(e, res);
                            res.code = EthUtils.bufferToHex(c);
                            return cb(null, res);
                        });
                    else return cb(err, res);
                });
            }
        )( cbFinal );    
    }
    isAccountExistInDb(address, blockNumber, cbFinal) {
        address=EthUtils.addHexPrefix(address);
        if(!EthUtils.isValidAddress(address)) throw TypeError('expecting a valid Ethereum address in string format');
        let addressB = EthUtils.toBuffer(address);
        if(!blockNumber) blockNumber='latest';
        if( typeof cbFinal !== 'function' ) throw new TypeError('Expecting a callback function(error, account)');

        let self = this;
        async.seq(
            cb=>{
                self._getVMCopyAtBlock(blockNumber, cb);
            },
            (vm, cb)=>{
                vm.stateManager._trie.get(addressB, (err, rawdata) => {
                    if( err ) return cb(err, null);
                    cb(null, rawdata != null)
                });
            }
        )( (err, exists)=>{
            if(err && err.toString().indexOf('NotFoundError')>=0) cbFinal(null, false)
            else cbFinal(err, exists)
        });    
    }
    proposeNextNonce(address, blockNumber, cb) {
        address=EthUtils.addHexPrefix(address);
        if(!EthUtils.isValidAddress(address)) throw TypeError('expecting a valid Ethereum address in string format');
        if(!blockNumber) blockNumber='latest';
        
        let lastNonce = this._pendingTransactions.getLastKnownNonce(address)
        if(lastNonce) setImmediate(()=>cb(null, lastNonce.addn(1)))
        else this.getAccount(address, false, blockNumber, (err, acc)=>{
            if(err) return cb(err)
            cb(null, acc.nonce)
        })
    }
    getBlock(hashOrNumber, withFullTransactions, cb) {
        if( typeof withFullTransactions !== 'boolean' ) {cb=withFullTransactions; withFullTransactions=false;}
        if( typeof cb !== 'function' ) throw new TypeError('Expecting a callback function(error, block)');

        if( hashOrNumber==='earliest')
            this.blockchain.blockchain.getBlock( new BN(0), processBlock);
        else if( hashOrNumber==='latest') 
            this.blockchain.blockchain.getLatestBlock(processBlock);
        else if(EthUtils.isValidHash256(hashOrNumber))
            this.blockchain.blockchain.getBlock( EthUtils.toBuffer(hashOrNumber), processBlock);
        else if(BN.isBN(hashOrNumber))
            this.blockchain.blockchain.getBlock( hashOrNumber, processBlock);
        else throw new TypeError('Expecting a valid 256 bits hash in hex string, a Big Number or a Tag');
        
        function processBlock(err, block) {
            if(err) return cb(err, null);
            cb(err, formatBlock(block, withFullTransactions));
        }
    }

    getTransaction(hash, cb) {
        if( typeof cb !== 'function' ) throw new TypeError('Expecting a callback function(error, tx)');
        if( !EthUtils.isValidHash256(hash) ) throw new TypeError('Expecting a valid 256 bits hash hex string');
        
        if( this._pendingTransactions.exists(hash) ) { // It is found in the pending db
            cb(null, formatPendingTransaction(this._pendingTransactions.getByHash(hash)))
        } else { // now try looking in the database
            this.blockchain.getTxBlockLookup(hash, (err, lookup) =>{
                if(err) return cb(err, null);
                this.getBlock( lookup.blockHash, true, (err, block) => {
                    if(err) cb(err);
                    else cb(null, block.transactions[lookup.index]);
                });
            });
        }
    }

    getTransactionInBlock(hashOrNumber, index, cb) {
        if( !BN.isBN(index) ) index = new BN(index);
        if( index.negative ) throw new TypeError('Expecting a positive index value')
        this.getBlock( hashOrNumber, true, (err, block) => {
            if(err) cb(err);
            else if(index<block.transactions.length) cb(null, block.transactions[index]);
            else cb(null, null);
        });
    }

    getTransactionReceipt(hash, cb) {
        if( typeof cb !== 'function' ) throw new TypeError('Expecting a callback function(error, tx)');
        if( !EthUtils.isValidHash256(hash) ) throw new TypeError('Expecting a valid 256 bits hash hex string');
        let self = this;
        let txIndex;
        async.seq(
            cb => self.blockchain.getTxBlockLookup(hash, cb),
            (lookup, cb) => {
                txIndex = lookup.index;
                self.blockchain.blockchain.getBlock( EthUtils.toBuffer(lookup.blockHash), cb)},
            (block, cb) => TransactionReceipts.loadFromDb(self.db, block, cb),
            (receipts, cb) => {
                cb(null,receipts.toWeb3Format(txIndex.toNumber()));
            }
        )(cb);
    }

    playTransaction(tx, blockNumber, cbFinal) {
        if( !tx || !(tx instanceof Transaction)) throw new TypeError('expecting a valid Transaction instance');
        //if( !tx.verifySignature() ) throw new TypeError('The transaction is not properly signed');
        if(!blockNumber) blockNumber='latest';
        if( typeof cbFinal !== 'function' ) cbFinal = ()=>{};
        let self = this;
        
        async.seq(
            cb=>{
                self._getVMCopyAtBlock(blockNumber, cb);
            },
            (vm, cb)=>{
                if(tx.gasLimit.length==0) tx.gasLimit = vm.gasLimit;
                vm.runTx({tx:tx, skipNonce:true, skipBalance:false}, cb);
            },
            (results, cb)=>{
                results.transaction=tx;
                //console.log('RunTx Result', formatPlayTransactionReceipt(results));
                cbFinal(null, formatPlayTransactionReceipt(results));
                cb()
            }
        )(error=>{ if(error) cbFinal(error, null) });
    }


    // TODO: Refactoring needed
    triggerBlockCreation() {this.addTransaction({justTriggerBlockCreation:true}, null)}
    addTransaction(tx, finalCb) {
        let self = this;
        if(tx && tx.justTriggerBlockCreation) // this is just to trigger the block, do not add transaction
            // IMPORTANT, must wait until the any ongoing block creation has completed
            // Only triggers the block creation if the mining is activated --> means TODO: handle pending transactions
            if(self.miningActivated) setImmediate( awaitingprocessPendingTransactionsWithVM )
            else ; // do nothing
        else { // check a proper transaction is received
            if( !tx || !(tx instanceof Transaction)) throw new TypeError('expecting a valid Transaction instance');
            if( !tx.verifySignature() ) throw new TypeError('The transaction is not properly signed');
            if( typeof finalCb !== 'function' ) finalCb = ()=>{};

            // NOTE Ordering: This is possibly where, in case the ethereum standalone node is used a private node in a privacy node
            //          that the account of the transaction should skip missing nonces, ie that account.nonce=tx.nonce
            //          Finally, modify pendingTransactions.push or pendingTransactions.popPending
            
            //console.log('addTransaction', tx.hash().toString('hex'))
            self._pendingTransactions.push(tx, (err, hash) =>{
                // IMPORTANT, must wait until the any ongoing block creation has completed
                // Only triggers the block creation if the mining is activated --> means TODO: handle pending transactions
                if(self.miningActivated) setImmediate( awaitingprocessPendingTransactionsWithVM )
                finalCb(err, hash)
            }) 
        
        }

        function awaitingprocessPendingTransactionsWithVM() {
            // init the mutex the first time and make it active
            if( !self.creatingBlock ) {self.creatingBlock = new Stoplight(); self.creatingBlock.go() }
            //console.log('awaitingprocessPendingTransactionsWithVM', self.creatingBlock.isLocked?'locked':'unlocked')
            self.creatingBlock.await(processPendingTransactionsWithVM)
        }

        function processPendingTransactionsWithVM() {
            // check if there is something to process because for the moment we do not want empty blocks
            if(self._pendingTransactions.firstPending(false)==null) return
            // if a call reaches this stage anyway, throw it back to where it came to make it wait
            if(self.creatingBlock.isLocked) return setImmediate(awaitingprocessPendingTransactionsWithVM)
            // activate the mutex on the processing to block other simultaneous processing request
            self.creatingBlock.stop();
            //console.log('STARTING MINING A BLOCK ....')
            
            // set the coinbase of the blockchain to impact the blocks created
            self.blockchain.coinbase = self.coinbase;

            // in asynchronous sequence
            async.seq(
                // 0. get the transactions to be processed in the right order and empty the pending map but keep the callbacks
                cb => self.blockchain.blockchain.getLatestBlock(cb),
                (blockHead, cb) => {
                    let gasLimit = new BN(blockHead.header.gasLimit)
                    cb(null, gasLimit)
                },
                (gasLimit, cb) => {
                    //console.log('GOT GAS LIMIT', gasLimit)
                    //let transactions = []
                    let totalGas = new BN(0)
                    async.doWhilst( 
                        cb=>self._pendingTransactions.popPending( (err, tx) => {
                            //console.log('GOT A TX TO PROCESS', err)
                            if(!tx) return cb(null, null)
                            // now check the gas
                            let txGas = new BN(tx.gasLimit)
                            if(totalGas.iadd(txGas).gt(gasLimit)) { 
                                // no space left, resend the tx in the pending and stop
                                //console.log('TOTAL GAS NOT OK')
                                self._pendingTransactions.push(tx, (err, hash)=>cb(null,null) )
                            } else {
                                //console.log('TOTAL GAS OK')
                                //transactions.push(tx) // add this transaction
                                cb(null, tx)
                            }
                        }),
                        (tx)=>!!tx, // continue if the last is a valid transaction
                        err=>{
                            //console.log('TRANSACTIONS SELECTED', self._pendingTransactions.getAllProcessing().length)
                            cb(err)
                        }
                    ) 
                },
                // 1. Add transactions into a new block added to the blockchain 
                (cb) => { // this is not satisfactory to use "vm.stateManager._trie" to only set the header stateRoot
                    self.blockchain.putNewTransactionsInBLock(self.vm.stateManager._trie, self._pendingTransactions.getAllProcessing(), cb);
                },
                (block, cb) => {
                    //console.log('BEFORE RUNNING VM' )
                    cb(null, block)
                },
                // 2. Process the transactions in the new block into the VM to generate the new StateTrie
                (block, cb) => self.vm.runBlockchain((err, results) => {
                    //console.log('AFTER RUNNING VM')
                    cb(err, results);
                }),
                // result is an array (for each block) of array (for each tx) of results
                (results, cb) => {
                    // 1. persists new blocks (again) so the correct blockhash is stored
                    async.forEachSeries(results,
                        (blockResult, blockCb) => {
                            if( blockResult.length>0 ) 
                                self.blockchain.persistBlock(blockResult[0].block, err=>{
                                    if(err) console.log("Fail to persist block after VM processing!", blockResult[0].block.hash().toString('hex'));
                                    blockCb(err);
                                });
                            else blockCb('Not a valid block result') // THIS HAPPENS WHEN THE BLOCK IS EMPTY!!
                            },
                        (err) => {
                            if(!err) self._pendingTransactions.clearAllProcessing()
                            cb(err, results)
                        } );    
                },
                (results, cb) => {
                    // 1 bis. Force an iteration over the blockchain to force proper saving of the _heads['vm']
                    self.blockchain.blockchain.iterator('vm', (block, reorg, cb)=>cb(), (err) => {
                        cb(err, results)
                    })
                },
                (results, cb) => {
                    // 2. prepare the result and send it to the kept callback
                    //console.log('NUMBER OF BLOCKS', results.length)
                    results.forEach( (blockResult) => {
                        let tr = TransactionReceipts.fromBlockResult(blockResult);
                        console.log("NEW BLOCK:", new BN(tr.block.header.number).toString(10), tr.block.header.hash().toString('hex'), 'Tx count:', tr.block.transactions.length);
                        self.emit('block', tr.block )
                        tr.logsIteratorSync( formattedLog=>{
                            self.emit('log', formattedLog) 
                        })
                    });
                    cb();
                },
            ) (error => {
                if(error) {
                    // error means that the new block has not been created
                    console.log('Adding transaction in block', error?'failure:':'success', (error?error:'')); 
                } 
                // release a request waiting to process its transactions
                //console.log('Release mutex')
                self.creatingBlock.go();
            });
        }

    }

    getStorageAt(address, position, blockNumber, finalCb) {
        address=EthUtils.addHexPrefix(address);
        if(!EthUtils.isValidAddress(address)) throw TypeError('expecting a valid Ethereum address in string format');
        position=EthUtils.addHexPrefix(position);
        if(!EthUtils.isValidHash256) throw TypeError('expecting a valid 256bits big number in hex string format');
        if(!blockNumber) blockNumber='latest';

        if( typeof finalCb !== 'function' ) finalCb = ()=>{};

        let self = this;
        async.seq(
            cb=>{
                self._getVMCopyAtBlock(blockNumber, cb);
            },
            (vm, cb)=>{
                let key = new BN(EthUtils.toBuffer(position)).toArrayLike(Buffer, 'be', 32);
                //console.log('getStorageAt key', key.toString('hex'))
                vm.stateManager.getContractStorage(address, key, cb);
            }
        )(finalCb);
    }

    _getVMCopyAtBlock(blockNumber, finalCb) {
        if(!blockNumber) blockNumber='latest';

        if( typeof finalCb !== 'function' ) finalCb = ()=>{};

        let self = this;
        async.seq(
            cb=>{
                if(blockNumber==='latest') self.blockchain.blockchain.getLatestBlock(cb);
                else self.blockchain.blockchain.getBlock(blockNumber,cb);
            },
            (block, cb)=>{
                let stateRoot = block.header.stateRoot;
                let vm = self.vm.copy();
                vm.gasLimit = block.header.gasLimit;
                vm.stateManager.setStateRoot(stateRoot, err=>{
                    if(err) cb(err,null);
                    else cb(null, vm);
                });
            }
        )(finalCb);
    }
    blockIterator(fromBlock, toBlock, onBlock, onCompletion) {
        if(!BN.isBN(fromBlock) || !BN.isBN(toBlock)) throw TypeError('expects fromBlock and toBlock to be big numbers')
        if(typeof onBlock!=='function') onBlock=(b, next)=>{}
        if(typeof onCompletion!=='function') onCompletion=(err)=>{}
        
        // check the order
        if(toBlock.lt(fromBlock)) toBlock=fromBlock.clone()
        let blockIndex = fromBlock.clone()
        let self = this
        async.doWhilst(processBlock, ()=>blockIndex.lte(toBlock), completed)
        function processBlock(cb) {
            self.blockchain.blockchain.getBlock(blockIndex, (err, block)=>{
                let msg = (err? err.toString() : '')
                if(err) console.log('Debug iterator: at Block'+blockIndex.toNumber(), msg)
                if(err && msg.indexOf('NotFoundError')<0) return cb(err) // an error that is not 'NotFoundError'
                // tells the caller that it has a block ready and ignore any return or error
                blockIndex.iaddn(1)
                if(block) onBlock(block, cb)
            })
        }
        function completed(err) {
            // tells the caller if there is an error and at what block it stopped
            if(!err) blockIndex.iaddn(-1)
            setImmediate(()=>onCompletion(err, blockIndex))
        }
    }
    close(cb) {
        // stop new mining
        // wait all mining completed
        // stop keystore
        // persist pending transactions if any
        // close the db
        if(!this._initialized) return cb()
        this._initialized=false
        let self = this
        async.seq(
            cb=>{
                self.miningActivated=false
                if(self.creatingBlock)
                    self.creatingBlock.await(cb)
                else cb()
            },
            cb=> {if(self.keystore) self.keystore.stop(cb)},
            cb=> {
                if(!self._pendingTransactions) return cb()
                console.log('Persisting pending transactions')
                let pendings = self._pendingTransactions.serialize()
                console.log('Pending data', EthUtils.bufferToHex(pendings))
                self.db.put(pendingTransactionsKey, EthUtils.bufferToHex(pendings), cb)
            },
            cb=> {
                console.log('Try closing the database')
                self.db.close(cb)
            }
        )(cb)
    }

    graceFullStop(cb) { // DEPRECATED, use close()
        this.close(cb)
    }
}


module.exports = EthereumStandaloneNode;
module.exports.formatBlock = formatBlock