var chai = require('chai');  
var assert = chai.assert;    // Using Assert style
var expect = chai.expect;    // Using Expect style
var should = chai.should();  // Using Should style


const async = require('async');
const EthSN = require('../index.js');
const EthUtils = require('ethereumjs-util');
const BN = EthUtils.BN;
const Transaction = require('ethereumjs-tx')
const cleaner = require('../lib/util/store-cleaner')
const PendingTransactions = require('../lib/pendingTransactions')
const Wallet = require('ethereumjs-wallet');
const Account = require('ethereumjs-account').default;


EthUtils.isValidHash256 = function (hash) {
    return (/^0x[0-9a-fA-F]{64}$/.test(hash)
    );
  };

describe('Ethereum Standalone Node - Pending', function() {
    const dbPath = './test/db/pending'
    let esn, wallet1, wallet2
    let serializedData
    after(function(){cleaner.cleanLevelDbSync(dbPath)})
    before((done) => {
        wallet1 = Wallet.generate()
        wallet2 = Wallet.generate()
        cleaner.cleanLevelDbSync(dbPath)
        esn = new EthSN({dbPath:dbPath, keystorePath:'memory', networkId:6000});
            
        esn.on('block', block=>console.log('NEW BLOCK', new BN(block.header.number).toNumber(), block.hash().toString('hex'), 'Nb tx:', block.transactions.length))
        esn.on('block', block=>console.log('----------------------------------------------------------------------------------------------') )
        //esn.on('log', log=>console.log('New Log', log))
        async.seq(
            cb=>esn.initialization(()=>{
                esn.should.have.property('db').that.is.not.null;
                esn.should.have.property('blockchain').that.is.not.null;
                esn.should.have.property('vm').that.is.not.null;
                cb()
            }),
            cb=>esn.vm.stateManager.putAccount(wallet1.getAddress(), new Account({balance:10000000000000000000}), cb),
            cb=>esn.vm.stateManager.putAccount(wallet2.getAddress(), new Account({balance:10000000000000000000, nonce:3}), cb),
            
        )(err=>{
            expect(err).to.be.null
            done()
        })


    });


    describe('create pending Transactions object', () => {
        let pt
        before(() => {
            pt = new PendingTransactions(esn.vm.stateManager)
        });
        it('should have expected properties', () => {
            expect(pt).to.have.property('pending')
            expect(pt).to.have.property('queued')
        });
        it('should add a first transaction in pending', (done) => {
            let tx = new Transaction({from:wallet1.getAddressString(), to:wallet2.getAddressString(), value: 1000, gas:22000, gasPrice:10})
            tx.sign(wallet1.getPrivateKey())
            pt.push(tx, (err, hash) => {
                console.log('Inspection:\n',pt.inspect())
                expect(err).to.be.null
                expect(EthUtils.isValidHash256(hash)).to.be.true
                expect(pt.pending).to.have.property(wallet1.getAddressString())
                expect(pt.queued).to.be.empty
                expect( pt.exists(hash) ).equal('pending')
                done()
            })
        });
        it('should add a 2nd transaction in queued', (done) => {
            let tx = new Transaction({from:wallet1.getAddressString(), to:wallet2.getAddressString(), value: 1000, gas:22000, gasPrice:1000, nonce:5})
            tx.sign(wallet1.getPrivateKey())
            pt.push(tx, (err, hash) => {
                expect(err).to.be.null
                expect(EthUtils.isValidHash256(hash)).to.be.true
                console.log('Inspection:\n',pt.inspect())
                expect(pt.pending).to.have.property(wallet1.getAddressString())
                expect(pt.queued).to.have.property(wallet1.getAddressString())
                done()
            })
        });

        it('should add a first transaction in queued', (done) => {
            let tx = new Transaction({from:wallet2.getAddressString(), to:wallet1.getAddressString(), value: 1000, gas:22000, gasPrice:1000, nonce:4})
            tx.sign(wallet2.getPrivateKey())
            pt.push(tx, (err, hash) => {
                expect(err).to.be.null
                expect(EthUtils.isValidHash256(hash)).to.be.true
                console.log('Inspection:\n',pt.inspect())
                expect(pt.pending).to.not.have.property(wallet2.getAddressString())
                expect(pt.queued).to.have.property(wallet2.getAddressString())
                done()
            })
        });

        it('should add a transaction in pending moving queued ones in pending', (done) => {
            let tx = new Transaction({from:wallet2.getAddressString(), to:wallet1.getAddressString(), value: 1000, gas:22000, gasPrice:100, nonce:3})
            tx.sign(wallet2.getPrivateKey())
            pt.push(tx, (err, hash) => {
                expect(err).to.be.null
                expect(EthUtils.isValidHash256(hash)).to.be.true
                console.log('Inspection:\n',pt.inspect())
                expect(pt.pending).to.have.property(wallet2.getAddressString())
                const v = pt.pending[wallet2.getAddressString()]
                expect(Object.keys(v).length).to.equals(2)
                expect(pt.queued).to.not.have.property(wallet2.getAddressString())
                done()
            })
        });


        it('should reject a transaction without enough funds', (done) => {
            let tx = new Transaction({from:wallet2.getAddressString(), to:wallet1.getAddressString(), value: 1000, gas:22000, gasPrice:10000000000000000000, nonce:3})
            tx.sign(wallet2.getPrivateKey())
            pt.push(tx, (err, hash) => {
                expect(err).to.contains('sender doesn\'t have enough funds to send tx')
                expect(hash).to.be.null
                done()
            })
        });
        it('should reject a transaction without enough gas for the min gas', (done) => {
            let tx = new Transaction({from:wallet2.getAddressString(), to:wallet1.getAddressString(), value: 1000, gas:20000, gasPrice:10000, nonce:3})
            tx.sign(wallet2.getPrivateKey())
            pt.push(tx, (err, hash) => {
                expect(err).to.contains('base fee exceeds gas limit ')
                expect(hash).to.be.null
                done()
            })
        });
        it('should reject a transaction without a too low nonce', (done) => {
            let tx = new Transaction({from:wallet2.getAddressString(), to:wallet1.getAddressString(), value: 1000, gas:22000, gasPrice:1000, nonce:0})
            tx.sign(wallet2.getPrivateKey())
            pt.push(tx, (err, hash) => {
                expect(err).to.contains('the tx doesn\'t have the correct nonce')
                expect(hash).to.be.null
                done()
            })
        });
        it('should retrieve the pending and queued transactions as json', () => {
            console.log('JSON object structure:\n', JSON.stringify(pt.toJSON(),null,4))
        });
        it('should be able to serialize the transactions', () => {
            serializedData = pt.serialize()
            console.log('Array RLP format serialized:\n', EthUtils.bufferToHex(serializedData))
        });
        it('should retrieve the tx in order', (done) => {
            async.doWhilst( 
                cb=>pt.popPending( (err, tx) => {
                    if(tx) console.log('Tx:', EthUtils.bufferToHex(tx.hash()), new BN(tx.gasPrice).toString(), EthUtils.bufferToHex(tx.from), new BN(tx.nonce).toString() )
                    cb(null, tx)
                }),
                tx => !!tx ,
                err=>{
                    console.log('Inspection:\n',pt.inspect())
                    done()
                }
            ) 
        });

        it('should succeed a stateChange when nothing changed', (done) => {
            pt.stateChanged(()=>{
                console.log('Inspection:\n',pt.inspect())
                done()
            })
        });
        it('should move queued tx to pending after a state change', (done) => {
            async.seq(
                cb=>esn.vm.stateManager.putAccount(wallet1.getAddress(), new Account({balance:10000000000000000000, nonce:5}), cb),
                cb=>pt.stateChanged(cb),
            )(err=>{
                expect(err).to.be.null
                console.log('Inspection:\n',pt.inspect())
                done()
            })
        });
    });

    describe('create pending Transaction object from serialized data', () => {
        let pt
        before(() => {
            pt = new PendingTransactions(esn.vm.stateManager)
        });
        it('it should have the correct data', (done) => {
            pt.initFromData(serializedData, (err, results)=>{
                expect(err).to.be.null
                console.log('Inspection:\n',pt.inspect())
                console.log('Init results:\n', results)
                expect(Object.keys(pt.pending).length).equal(2)
                expect(Object.keys(pt.queued).length).equal(0)
                done()
            })
        });
    });
})


