const EthSN = require('../index.js');
const Web3 = require('web3')
require('ethereum-web3-plus')
const Web3Provider = require('../lib/rpc/web3-provider')
const cleaner = require('../lib/util/store-cleaner')

var chai = require('chai');  
var assert = chai.assert;    // Using Assert style
var expect = chai.expect;    // Using Expect style
var should = chai.should();  // Using Should style


describe('Testing using web3 provider on the Ethereum Standalone Node', () => {
    let web3 
    let coinbase
    let sm
    before((done) => {
        let dbPath= __dirname+'/db/web3'
        cleaner.cleanLevelDbSync(dbPath)
        let esn = new EthSN({dbPath:dbPath, keystorePath:'memory', networkId:6000});
        esn.initialization(()=>{
            esn.should.have.property('db').that.is.not.null;
            esn.should.have.property('blockchain').that.is.not.null;
            esn.should.have.property('vm').that.is.not.null;
            web3 = new Web3( esn.web3Provider(true) )
            done()
        })

    });
    it('should be able to get the version', (done) => {
        web3.version.getNode((err, value)=>{
            
            done()
        })
    });
    it('should be able to get the block number', (done) => {
        web3.eth.getBlockNumber((err, value)=>{
            console.log('BlockNumber=', value)
            done()
        })
    });
    it('should create a new coinbase and unlock it', function(done) {
        this.timeout(3000)
        web3.personal.importRawKey('0xceee921f6252f389fe940329b3c563fb1dbedf6bbd8cf31cb694274916b35bd6', 'password', (error, address)=>{
            web3.personal.unlockAccount(address, 'password', 0 , (error, state)=>{
                //console.log(error, address, state)
                expect(state).to.be.true
                coinbase=address
                done()    
            })
        })
    });
    it('should be able to create the smart contract with the web3 library', function(done) {
        const contractName = 'PrivateTxStorage'
        let compiler = web3.solidityCompiler()
        let instance = compiler.getContract(contractName)
        let bytecode = compiler.getCode(contractName) 
        instance.new({data:bytecode, from:coinbase, gasLimit:1000000, gasPrice:0}, 
            (error, instance)=>{ // is called multiple times, once after the tx hash is retreived, one after the address is available 
                //console.log(error, instance.address)
                if(!instance.address) return
                expect(error).to.be.null
                expect(instance).to.not.be.null
                expect(instance.address).match(/^0x[0-9a-fA-F]{40}$/)
                sm=instance
                done()
            })
    });
    it('should be able to call the smart contract', (done) => {
        sm.setEncryptedTransaction('0x1234', 'some data', {from:coinbase}, (err, txHash)=>{
            console.log('setEncryptedTransaction:', err, txHash)
            setTimeout( ()=>{
                web3.eth.getTransactionReceipt(txHash, (err, receipt)=>{
                    console.log('Receipt:', receipt)
                    done()
                })
            }, 500)

        })
    });
});