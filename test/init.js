var chai = require('chai');  
var assert = chai.assert;    // Using Assert style
var expect = chai.expect;    // Using Expect style
var should = chai.should();  // Using Should style


const async = require('async');
const EthSN = require('../index.js');
const BN = require('ethereumjs-util').BN;
const fs = require('fs');
const cleaner = require('../lib/util/store-cleaner')

describe('Initializing with test data', function(){
    const testData = require('./bcInit/bcRPC_API_Test.json');
    const ESN = require('../index.js');

    const dbPath = './test/db/init'
    let esn = null
    let appHandler;
    
    after(function(){cleaner.cleanLevelDbSync(dbPath)})

    before(() => {
        cleaner.cleanLevelDbSync(dbPath)
        esn = new ESN({dbPath:dbPath, keystorePath:'memory'});
    });


    it('should initialize', function(done) {
        this.timeout(10000);
        esn.initializationWithTestData(testData,()=>{
            appHandler = esn.jsonRpcHandler();
            appHandler.trace=true;
            done();
        });
    })
    it('should be able to save the generated blocks', function(done) {
        testData.should.have.property('generatedBlocks');
        fs.writeFile('./test/bcInit/bcRPC_API_Test_Generated.json', JSON.stringify(testData.generatedBlocks,null, 4), done)
    })
    it('should have the last block as expected', function(done){
        esn.getBlock('latest', false, function(error, block){
            testData.lastblockhash='0xc3de2661108ef7f1068e2deb7ffd83423397efc4368589f2902b6d2786a7b2e4';
            testData.lastblocknumber='0x20'
            expect(error).to.be.null;
            block.number.should.equal(testData.lastblocknumber);
            block.hash.should.equal(testData.lastblockhash);
            done();
        });

    })
    it('should handle errors corectly', function(done) {
        const req={ id: 139,
            jsonrpc: '2.0',
            method: 'eth_getBlockByHash',
            params: 
             [ '0x878a132155f53adb7c993ded4cfb687977397d63d873fcdbeb06c18cac907a5c',
               true ] }
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        } )
    })

    it('should find a transaction by hash', function(done) {
        const req={ id: 139,
            jsonrpc: '2.0',
            method: 'eth_getTransactionByHash',
            params: 
             [ '0x812742182a79a8e67733edc58cfa3767aa2d7ad06439d156ddbbb33e3403b4ed'] }
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        } )
    })
    it('should find a transaction receipt', function(done) {
        const req={ id: 4,
            jsonrpc: '2.0',
            method: 'eth_getTransactionReceipt',
            params: 
             [ '0xb1a62356d1433202cdef0ef9030f8abdfbb3aef549fab0867cf0eaee70b09d81' ] }
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        } ) 
    })
    it('should return the proper UInt256 value', function(done){
        const req={ id: 16,
            jsonrpc: '2.0',
            method: 'eth_call',
            params: 
             [ { to: '0x6295ee1b4f6dd65047762f924ecd367c17eabf8f',
                 data: '0x68895979' },
               '0x1e' ] }
        // !!! DOES NOT RETURN THE CORRECT VALUE WHEN IN MEM LELEVDB
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        } ) 
    })
    it('should return the proper Int256 value', function(done){
        const req={ id: 16,
            jsonrpc: '2.0',
            method: 'eth_call',
            params: 
             [ { to: '0x6295ee1b4f6dd65047762f924ecd367c17eabf8f',
                 data: '0xf5b53e17' },
               '0x1e' ] }
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        } ) 
    })

    it('should be able to iterate thru block', (done) => {
        esn.blockIterator(new BN(5), new BN(15), (block, next)=>{
            console.log('Iterator on block', block.header.number)
            next()
        }, (err,lastNumber)=>{
            console.log('Iterator completed on block', lastNumber, 'with error', err)
            done()
        })
    });
});

