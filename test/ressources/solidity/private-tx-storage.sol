pragma solidity ^0.5.1;

contract PrivateTxStorage {
    
    event newPrivateTransaction(bytes32 indexed relatedTxHash, string rsaYourKeyBase64);
    mapping(bytes32=>string) encryptedTransactions;
    mapping(bytes32=>bool) encryptedTransactionExists;

    function getEncryptedTransaction(bytes32 id) public view returns (string memory encryptedTx) {
        require(encryptedTransactionExists[id], "id must exists");
        return encryptedTransactions[id];
    }

    function setEncryptedTransaction(bytes32 id, string memory encryptedTx) public {
        require(!encryptedTransactionExists[id], "id must not exists");
        encryptedTransactions[id] = encryptedTx;
        encryptedTransactionExists[id] = true;
    }

    function notify(bytes32 relatedTxHash, string memory rsaEncryptedKey) public {
        emit newPrivateTransaction(relatedTxHash, rsaEncryptedKey);
    }
}