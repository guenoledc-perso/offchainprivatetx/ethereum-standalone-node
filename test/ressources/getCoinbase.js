const Wallet = require('ethereumjs-wallet');
const EthUtil = require('ethereumjs-util');

module.exports = Wallet.fromPrivateKey(EthUtil.toBuffer('0xceee921f6252f389fe940329b3c563fb1dbedf6bbd8cf31cb694274916b35bd6'));
