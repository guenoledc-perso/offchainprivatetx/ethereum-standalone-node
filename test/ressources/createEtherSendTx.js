const Wallet = require('ethereumjs-wallet');
const EthUtil = require('ethereumjs-util');

const Transaction = require('ethereumjs-tx');

module.exports = function sendEther(fromWallet, toAddress, valueWei) {
    if(!fromWallet.lastNonce) 
        fromWallet.lastNonce=0;
    let data = {
        from: fromWallet.getAddressString(),
        to: toAddress,
        value: EthUtil.bufferToHex(EthUtil.toBuffer(valueWei)),
        nonce: fromWallet.lastNonce,
        gasPrice: '0x0', 
        gasLimit: '0xFFFFFFF00',//21000, //'0xFFFFFFF00'
        chainId: 6000
    };
    //console.log('Txdata', data);
    let tx = new Transaction(data);
    tx.sign(fromWallet.getPrivateKey());
    //console.log("TXHASH=", tx.hash().toString('hex'))
    fromWallet.lastNonce++;
    return tx;
}