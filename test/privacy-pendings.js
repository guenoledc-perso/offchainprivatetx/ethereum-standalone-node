var chai = require('chai');  
var assert = chai.assert;    // Using Assert style
var expect = chai.expect;    // Using Expect style
var should = chai.should();  // Using Should style


const async = require('async');
const EthSN = require('../index.js');
const EthUtils = require('ethereumjs-util');
const BN = EthUtils.BN;
const coinbaseWallet = require('./ressources/getCoinbase');
const cleaner = require('../lib/util/store-cleaner')

const EthFilterEngine = require('../lib/filters')

EthUtils.isValidHash256 = function (hash) {
    return (/^0x[0-9a-fA-F]{64}$/.test(hash)
    );
  };

describe('Ethereum Standalone Node - Privacy Pending', function() {
    let esn = null;
    let txHash = null
    let txReceipt = null;
    let filterEngine = null;
    let blockFilter = null

    const dbPath = './test/db/privacy-pendings'
    after(function(){cleaner.cleanLevelDbSync(dbPath)})

    before(function(){
        cleaner.cleanLevelDbSync(dbPath)
        esn = new EthSN({dbPath:dbPath, keystorePath:'memory', networkId:6000, inPrivacyNode:true});
        esn.coinbase = coinbaseWallet.getAddressString();

        esn.on('block', block=>console.log('NEW BLOCK', new BN(block.header.number).toNumber(), block.hash().toString('hex'), 'Nb tx:', block.transactions.length))
        esn.on('block', block=>console.log('----------------------------------------------------------------------------------------------') )
        esn.on('log', log=>console.log('New Log', log))

        filterEngine = new EthFilterEngine()
        blockFilter = filterEngine.add(filterEngine.newFilter(esn, EthFilterEngine.BLOCK_TYPE))
        console.log('Created filter on block: ', blockFilter)
    })
    describe('initialization', function() {
        it('should have a created db', function(){
            esn.should.have.property('db').that.is.not.null;
        });
        it('should initialize correctly', function(done) {
            esn.initialization(done);
        });
        it('should have valid properties', function(){
            esn.should.have.property('blockchain').that.is.not.null;
            esn.should.have.property('vm').that.is.not.null;
        });
    });


    describe('Processing transactions', function(){
        it('should accept smart contract creation', function(done){
            esn.addTransaction( require('./ressources/createContractTx'), 
            (err, hash) =>{
                console.log(hash);
                expect(err).to.be.null;
                expect(EthUtils.isValidHash256(hash)).to.be.true
                txHash = hash
                setTimeout( done, 500) ; // wait for block creation
            });
        });
        it('should find the created transaction hash in the blockchain', function(done){
            esn.getTransaction(txHash, (err, tx)=>{
                console.log(err, tx);
                expect(err).to.be.null
                tx.should.have.property('blockHash');
                tx.should.have.property('blockNumber');
                tx.should.have.property('hash', txHash);
                
                done();
            });
        });
        it('should have a latest blocknumber to 1', function() {
            let info = esn.getBlockchainInfo();
            info.blockNumber.toNumber().should.equal(1);
        });
        it('should have created the smart contract', function(done) {
            async.seq(
                cb=>esn.getTransactionReceipt(txHash, cb),
                (txReceipt, cb) => {
                    esn.getAccount(txReceipt.contractAddress, true, 'latest', function(err, account){
                    expect(err).to.be.null;
                    expect(account.code).should.not.equal('0x');
                    //console.log(account);
                    cb();
                })
            })(done)    
        });

    });

    describe('Processing send transactions', function(){
        // it('should wait one second to allow next timestamp to be valid', function(done){
        //     setTimeout(done, 1000);
        // });
        it('should have credited the coinbase', function(done) {
            esn.getAccount(coinbaseWallet.getAddressString(), false, 'latest', function(err, account){
                expect(err).to.be.null;
                account.balance.toString().should.equal('3000000000000000000');
                //console.log(account);
                done();
            });    
        });
        it('should accept several ether transfer transactions even with nonce gaps', function(done){
            let createEtherSendTx = require('./ressources/createEtherSendTx');
            let counter=0
            multiDone = () => {
                counter--;
                if(counter==0) done(); 
                }

            esn.addTransaction( createEtherSendTx(coinbaseWallet, '0x78e97bcc5b5dd9ed228fed7a4887c0d7287344a9', 10000000), 
            (err, hash) =>{
                //console.log("1",result);
                expect(err).to.be.null;
                multiDone();
            });
            counter++;
            coinbaseWallet.lastNonce += 3
            esn.addTransaction( createEtherSendTx(coinbaseWallet, '0x78e97bcc5b5dd9ed228fed7a4887c0d7287344a9', '0x100000000'), 
            (err, hash) =>{
                //console.log("2",result);
                expect(err).to.be.null;
                multiDone();
            });
            counter++;
        });

        it('should check the account Nonce after the last tx', (cb) => {
            // start by waiting the block processing
            setTimeout( ()=>{
                esn.getAccount(coinbaseWallet.getAddressString(),false, 'latest', (err, account)=>{
                    expect(err).to.be.null
                    expect( +EthUtils.bufferToHex(account.nonce) ).to.equal(coinbaseWallet.lastNonce)
                    cb()
                } )
            }, 500)
        });

    });
});
