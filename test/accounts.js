var chai = require('chai');  
var assert = chai.assert;    // Using Assert style
var expect = chai.expect;    // Using Expect style
var should = chai.should();  // Using Should style


const async = require('async');
const EthSN = require('../index.js');
const BN = require('ethereumjs-util').BN;
const fs = require('fs');
const cleaner = require('../lib/util/store-cleaner')


describe('Accounts keystore tests', function(){
    const testData = require('./bcInit/bcRPC_API_Test.json');
    const ESN = require('../index.js');

    const dbPath = './test/db/accounts'
    let esn = null
    let appHandler;
    let accounts=[];

    after(function(){cleaner.cleanLevelDbSync(dbPath)})

    before(() => {
        cleaner.cleanLevelDbSync(dbPath)
        esn = new ESN({dbPath:dbPath, keystorePath:'memory'});
    });
    it('should initialize', function(done) {
        this.timeout(10000);
        esn.initializationWithTestData(testData,()=>{
            appHandler = esn.jsonRpcHandler();
            appHandler.trace=true;
            done();
        });
    })

    it('should import a raw private key', function(done){
        const req={ id: 139,
            jsonrpc: '2.0',
            method: 'personal_importRawKey',
            params: 
             [ '0xceee921f6252f389fe940329b3c563fb1dbedf6bbd8cf31cb694274916b35bd6',
               'password' ] }
        appHandler.submitAsync(req, (response, status)=> {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        })
    })

    it('should unlock the account', function(done){
        const req={ id: 139,
            jsonrpc: '2.0',
            method: 'personal_unlockAccount',
            params: 
             [ '0x5dfc8b442c73fe215244a57f9aab5bee1822351e',
               'password', 3000 ] }
        appHandler.submitAsync(req, (response, status)=> {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        })
    })



    it('should sign a message', function(done) {
        const req={ id: 139,
            jsonrpc: '2.0',
            method: 'eth_sign',
            params: 
             [ '0x5dfc8b442c73fe215244a57f9aab5bee1822351e',
               'Some message of hex value' ] }
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        } )
    })
    it('should recover the address of the signatory', function(done) {
        const req={ id: 139,
            jsonrpc: '2.0',
            method: 'personal_ecRecover',
            params: 
             [ 'Some message of hex value',
                '0xfdce5f1ea881c5bcebf1ba0553df3f4b55254bfd36b81202dea66ec379b64f210e985c2fbc67d6cc04baa12f957dd775e1de0800f149170265f837b29a659d962f03' ] }
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        } )
    })



    it('should create a new account', function(done){
        const req={ id: 139,
            jsonrpc: '2.0',
            method: 'personal_newAccount',
            params: 
             [ 'password' ] }
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        } )

    })
    it('should list all accounts', function(done) {
        const req={ id: 139,
            jsonrpc: '2.0',
            method: 'personal_listAccounts',
            params: 
             [  ] }
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            accounts = response.result;
            console.log('Accounts:',accounts)
            done();
        } )
    })
    it('should send ether to another account with eth_sendTransaction', function(done) {
        const req={ id: 139,
            jsonrpc: '2.0',
            method: 'eth_sendTransaction',
            params: [ {
                from:'0x5dfc8b442c73fe215244a57f9aab5bee1822351e',
                to:accounts[1],
                value:'0x1000000',
                gasPrice:'0x0',
                nonce: 0
            } 
        ] }
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        } )
    })
    it('should send ether to another account with personal_sendTransaction', function(done) {
        const req={ id: 139,
            jsonrpc: '2.0',
            method: 'personal_sendTransaction',
            params: [ {
                from:'0x5dfc8b442c73fe215244a57f9aab5bee1822351e',
                to:accounts[1],
                value:'0x1000000',
                gasPrice:'0x0',
                nonce: 1
            }, 'password' 
        ] }
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        } )
    })

    it('should create a raw transaction with personal_signTransaction', function(done) {
        const req={ id: 139,
            jsonrpc: '2.0',
            method: 'personal_signTransaction',
            params: [ {
                from:'0x5dfc8b442c73fe215244a57f9aab5bee1822351e',
                to:accounts[1],
                value:'0x1000000',
                gasPrice:'0x0',
                nonce: 1
            }, 'password' 
        ] }
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        } )
    })

    it('should estimate gas', function(done){
        const req={ id: 16,
            jsonrpc: '2.0',
            method: 'eth_estimateGas',
            params: 
             [ { to: '0x6295ee1b4f6dd65047762f924ecd367c17eabf8f',
                 data: '0x68895979' },
               '0x1e' ] }
        // !!! DOES NOT RETURN THE CORRECT VALUE WHEN IN MEM LELEVDB
        appHandler.submitAsync(req, (response, status) => {
            expect(status).to.equal(200);
            expect(response).to.be.not.null;
            //console.log('Response:',response)
            done();
        } )     
    })
/*
    it('should wait for the processing to complete', function(done){
        this.timeout(10000);
        setTimeout(done, 9500)
    })
    */
})