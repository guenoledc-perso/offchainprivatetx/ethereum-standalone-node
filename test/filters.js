var chai = require('chai');  
var assert = chai.assert;    // Using Assert style
var expect = chai.expect;    // Using Expect style
var should = chai.should();  // Using Should style


const async = require('async');
const EthSN = require('../index.js');
const EthUtils = require('ethereumjs-util');
const BN = EthUtils.BN;
const coinbaseWallet = require('./ressources/getCoinbase');
const smartContract = require('./ressources/SmartContract')
const cleaner = require('../lib/util/store-cleaner')

let reqId = 0
let appHandler = null

function rpcCall(method, params, ready) {
    const req={ id: reqId++, jsonrpc: '2.0', method: method, params: params }
    appHandler.submitAsync(req, (response, status)=> {
        expect(status).to.equal(200);
        expect(response).to.be.not.null;
        expect(response).to.have.property('result')
        ready(response.result);
    })
}


describe('Ethereum Standalone Node - Filters', function() {
    //let esn = null;
    
    let blockFilter = null
    let logFilterAll = null
    let logFilterWithConditions = null

    const dbPath = './test/db/filters'
    after(function(){cleaner.cleanLevelDbSync(dbPath)})
    before(function(done){
        cleaner.cleanLevelDbSync(dbPath)
        let esn = new EthSN({dbPath:dbPath, keystorePath:'memory', networkId:6000});
        esn.coinbase = coinbaseWallet.getAddressString();
        coinbaseWallet.nonce=0;
            
        esn.on('block', block=>console.log('NEW BLOCK', new BN(block.header.number).toNumber(), block.hash().toString('hex'), 'Nb tx:', block.transactions.length))
        esn.on('block', block=>console.log('----------------------------------------------------------------------------------------------') )
        //esn.on('log', log=>console.log('New Log', log))

        esn.initialization(()=>{
            esn.should.have.property('db').that.is.not.null;
            esn.should.have.property('blockchain').that.is.not.null;
            esn.should.have.property('vm').that.is.not.null;
            appHandler = esn.jsonRpcHandler();
            appHandler.trace=true;
            done()
        });
    })
    describe('initialization', function() {
        it('rpc handler should be defined', function(){
            expect(appHandler).to.be.not.null
        });

    })
    describe('initialize filters', function() {
        it('should create a filter for blocks', (done) => {
            rpcCall('eth_newBlockFilter',[], result=>{
                blockFilter=result
                done()
            })
        });
        it('should create a filter for logs', (done) => {
            rpcCall('eth_newFilter',[{}], result=>{
                logFilterAll=result
                done()
            })
        });
        
    })

    describe('smart contract creation and its interactions', () => {
        let lastBlock=null
        let lastHash=null
        let contractAddress = null
        it('should create the smart contract', (done) => {
            let tx = smartContract.makeNewTx(null, coinbaseWallet, coinbaseWallet.nonce++)
            rpcCall('eth_sendRawTransaction', [EthUtils.bufferToHex(tx.serialize())], result=>{
                lastHash = result
                done()
            })
        });
        it('should wait for the transaction to be added in block', (done) => {
            let interval = setInterval( ()=>
                rpcCall('eth_getFilterChanges',[blockFilter], result=>{
                    if(result.length>0) {
                        clearInterval(interval)
                        lastBlock = result[0]
                        done()
                    }
                }), 200 )            
        });
        it('should get the created address from the transaction receipt', (done) => {
            rpcCall('eth_getTransactionReceipt', [lastHash], result=>{
                contractAddress = result.contractAddress;
                done()
            })
        });
        it('should send the set multiple transactions that generate logs', (done) => {
            let index = 0
            async.doWhilst((cb)=>{
                    let tx = smartContract.makeSetTx(contractAddress, 'value='+index, null, coinbaseWallet, coinbaseWallet.nonce++)
                    rpcCall('eth_sendRawTransaction', [EthUtils.bufferToHex(tx.serialize())], result=>{
                        lastHash=result
                        setTimeout( cb, 100)
                    })
                }, ()=>(index++)<9,
                (err)=>done()
            )

        });
    });

    describe('process logs', () => {
        it('should poll for logs', (done) => {
            let interval = setInterval( ()=>
                rpcCall('eth_getFilterChanges',[logFilterAll], result=>{
                    expect(result).is.not.null
                    expect(result.length).is.equal(10)
                    if(result.length>0) {
                        console.log('log(s) received via rpc', result)
                        clearInterval(interval)
                        done()
                    }
                }), 50)
        });

        it('should be able to get history of logs', function(done){
            rpcCall('eth_newFilter',[{
                fromBlock:0, 
                toBlock:'latest',
                address:['0xb32897619ba1cbdadbc5142362e2a0bb080374d0','0xb32897619ba1cbdadbc5142362e2a0bb080374d9'], 
                topics:['0x70b8fe7843281b24ae760421a90c0172b3b1a3f98b684f38aa9fb0456472895b', ['0xe7a006827de084c45d5bb588786876e4e03766476b01eea5a791741df745e699','0xd936043bf91ae15cefe422e6eae8f84e5a4378dfa0408e373d98e8b777b65744','0x9dfae6b94ca88b407b4c7527ed055539cdd199c8fc9b00798feff94e9b6eb0ae']]
            }], 
            result=>{
                logFilterWithConditions=result
                setTimeout( ()=> 
                    rpcCall('eth_getFilterChanges',[logFilterWithConditions], result=>{
                        expect(result).is.not.null
                        expect(result.length).to.equal(3)
                        if(result.length>0) {
                            console.log('log(s) history matching condition', result)
                            done()
                        } else done()
                    }), 100)
            })
            
        })
        it('should be able to retrieve all logs of the same filter', (done) => {
            rpcCall('eth_getFilterLogs', [logFilterWithConditions], result=>{
                console.log('All log(s) matching condition', result)
                done()
            })
        });
        it('should be able to get logs directly with options without creating filter', (done) => {
            rpcCall('eth_getFilterChanges',[blockFilter], result=>{
                if(result.length>0) {
                    rpcCall('eth_getLogs', [{
                        blockhash: result[result.length-1],
                        address:'0xb32897619ba1cbdadbc5142362e2a0bb080374d9', 
                        topics:[]
                    }], result=>{
                        expect(result).to.be.not.null
                        expect(result.length).to.equal(1)
                        console.log('All log(s) matching conditions without creating new filter', result)
                        done()
                    })
                } else done()
            })
            
        });
    });
});
