var chai = require('chai');  
var assert = chai.assert;    // Using Assert style
var expect = chai.expect;    // Using Expect style
var should = chai.should();  // Using Should style


const async = require('async');
const EthSN = require('../index.js');
const EthUtils = require('ethereumjs-util');
const BN = EthUtils.BN;
const coinbaseWallet = require('./ressources/getCoinbase');
const cleaner = require('../lib/util/store-cleaner')

const EthFilterEngine = require('../lib/filters')

EthUtils.isValidHash256 = function (hash) {
    return (/^0x[0-9a-fA-F]{64}$/.test(hash)
    );
  };

describe('Ethereum Standalone Node - Full', function() {
    let esn = null;
    let txHash = null
    let txReceipt = null;
    let filterEngine = null;
    let blockFilter = null

    const dbPath = './test/db/full'
    after(function(){cleaner.cleanLevelDbSync(dbPath)})

    before(function(){
        cleaner.cleanLevelDbSync(dbPath)
        esn = new EthSN({dbPath:dbPath, keystorePath:'memory', networkId:6000});
        esn.coinbase = coinbaseWallet.getAddressString();

        esn.on('block', block=>console.log('NEW BLOCK', new BN(block.header.number).toNumber(), block.hash().toString('hex'), 'Nb tx:', block.transactions.length))
        esn.on('block', block=>console.log('----------------------------------------------------------------------------------------------') )
        esn.on('log', log=>console.log('New Log', log))

        filterEngine = new EthFilterEngine()
        blockFilter = filterEngine.add(filterEngine.newFilter(esn, EthFilterEngine.BLOCK_TYPE))
        console.log('Created filter on block: ', blockFilter)
    })
    describe('initialization', function() {
        it('should have a created db', function(){
            esn.should.have.property('db').that.is.not.null;
        });
        it('should initialize correctly', function(done) {
            esn.initialization(done);
        });
        it('should have valid properties', function(){
            esn.should.have.property('blockchain').that.is.not.null;
            esn.should.have.property('vm').that.is.not.null;
        });
    });

    describe('reading blockchain information', function() {
        it('should return a proper blockchain info structure', function(){
            let info = esn.getBlockchainInfo();
            info.should.have.property('version', 6000);
            info.should.have.property('listening', false);
            info.should.have.property('peerCount', 0);
            info.should.have.property('protocolVersion', 64);
            info.should.have.property('syncing', false);
            info.should.have.property('coinbase','0x5dfc8b442c73fe215244a57f9aab5bee1822351e');
            info.should.have.property('gasPrice', '0x00');    
            info.should.have.property('blockNumber');
            info.blockNumber.toNumber().should.equal(0);
        });
    });

    describe('Accounts reading', function(){
        it('must not accept incorrect parameters', function(){
            expect( ()=>esn.getAccount('0x1234', ()=>{}) ).to.throw();
            expect( ()=>esn.getAccount('0x78e97bcc5b5dd9ed228fed7a4887c0d7287344a9') ).to.throw();
            expect( ()=>esn.getAccount('0x78e97bcc5b5dd9ed228fed7a4887c0d7287344a9'), true, null ).to.throw();
            expect( ()=>esn.getAccount('0x78e97bcc5b5dd9ed228fed7a4887c0d7287344a9'), null ).to.throw();
        });
        it('must return empty for valid random address', function(done){
            esn.getAccount('0x78e97bcc5b5dd9ed228fed7a4887c0d7287344a9',false, 'latest', function(err, account){
                expect(err).to.be.null;
                account.address.should.equal('0x78e97bcc5b5dd9ed228fed7a4887c0d7287344a9');
                account.balance.toNumber().should.equal(0);
                account.nonce.toNumber().should.equal(0);
                account.isContract.should.equal(false);
                account.code.should.equal('0x');
                done();
            });
        });
        it('must retrieve the precompiled contracts', function(done){
            esn.getAccount('0x0000000000000000000000000000000000000001',true, null, function(err, account){
                expect(err).to.be.null;
                account.address.should.equal('0x0000000000000000000000000000000000000001');
                account.balance.toNumber().should.equal(0);
                account.nonce.toNumber().should.equal(0);
                account.isContract.should.equal(false);
                account.code.should.equal('0x');
                done();
            });
        });
    });

    describe('Block reading', function() {
        it('should not accept invalid parameters', function(){
            expect( ()=>{esn.getBlock('0x000000234', ()=>{})} ).to.throw();
            expect( ()=>{esn.getBlock('0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331')} ).to.throw();
        });
        it('should retrieve the genesis block', function(done){
            esn.getBlock('latest', false, function(error, block){
                expect(error).to.be.null;
                block.number.should.equal('0x0');
                // below value is static for the given parameters
                block.timestamp.should.equal('0x5c5f1399');
                // Below for a secure merkle-patricia-trie
                block.hash.should.equal('0x8a4e8fb44a15082d69c63da6ac6052c2e1be592fab0a2574641ffbb9d9899371');
                // Below for a NOT secure merkle-patricia-trie
                //block.hash.should.equal('0xfb5c3857c52f633feeafd985c8c0ac6a2d3509b70549e500ce94f01cb2085592');
                //console.log(block);
                done();
            });
        });
    });

    describe('Transaction reading', function() {
        it('should not accept invalid parameters', function(){
            expect( ()=>{esn.getTransaction('0x000000234', ()=>{})} ).to.throw();
            expect( ()=>{esn.getTransaction('0x5d9e77a8c5380299fd72b267c2bf199af9c7bd96363c2c1a54873c2161fdac29')} ).to.throw();
        });
        it('should not retrieve a transaction with innexistant hash', function(done){
            esn.getTransaction('0x5d9e77a8c5380299fd72b267c2bf199af9c7bd96363c2c1a54873c2161fdac29', function(error, tx){
                expect(error).to.have.property('notFound');
                expect(tx).to.be.null;
                done();
            });
        });
    });

    describe('Processing transactions', function(){
        it('should not accept invalid parameters', function(){
            expect( ()=>{esn.addTransaction(null, null)} ).to.throw;
            expect( ()=>{esn.addTransaction({number:1234}, null)} ).to.throw;
        });
        it('should accept smart contract creation', function(done){
            esn.addTransaction( require('./ressources/createContractTx'), 
            (err, hash) =>{
                console.log(hash);
                expect(err).to.be.null;
                expect(EthUtils.isValidHash256(hash)).to.be.true
                txHash = hash
                setTimeout( done, 200) ; // wait for block creation
            });
        });
        it('should find the created transaction hash in the blockchain', function(done){
            esn.getTransaction(txHash, (err, tx)=>{
                console.log(err, tx);
                expect(err).to.be.null
                tx.should.have.property('blockHash');
                tx.should.have.property('blockNumber');
                tx.should.have.property('hash', txHash);
                
                done();
            });
        });
        it('should have a latest blocknumber to 1', function() {
            let info = esn.getBlockchainInfo();
            info.blockNumber.toNumber().should.equal(1);
        });
        it('should have created the smart contract', function(done) {
            async.seq(
                cb=>esn.getTransactionReceipt(txHash, cb),
                (txReceipt, cb) => {
                    esn.getAccount(txReceipt.contractAddress, true, 'latest', function(err, account){
                    expect(err).to.be.null;
                    expect(account.code).should.not.equal('0x');
                    //console.log(account);
                    cb();
                })
            })(done)    
        });

    });

    describe('Processing send transactions', function(){
        // it('should wait one second to allow next timestamp to be valid', function(done){
        //     setTimeout(done, 1000);
        // });
        it('should have credited the coinbase', function(done) {
            esn.getAccount(coinbaseWallet.getAddressString(), false, 'latest', function(err, account){
                expect(err).to.be.null;
                account.balance.toString().should.equal('3000000000000000000');
                //console.log(account);
                done();
            });    
        });
        it('should accept several ether transfer transactions', function(done){
            let createEtherSendTx = require('./ressources/createEtherSendTx');
            let counter=0
            multiDone = () => {
                counter--;
                if(counter==0) done(); 
                }

            esn.addTransaction( createEtherSendTx(coinbaseWallet, '0x78e97bcc5b5dd9ed228fed7a4887c0d7287344a9', 10000000), 
            (err, hash) =>{
                //console.log("1",result);
                expect(err).to.be.null;
                multiDone();
            });
            counter++;
            esn.addTransaction( createEtherSendTx(coinbaseWallet, '0x78e97bcc5b5dd9ed228fed7a4887c0d7287344a9', '0x100000000'), 
            (err, hash) =>{
                //console.log("2",result);
                expect(err).to.be.null;
                multiDone();
            });
            counter++;
        });

        it('should retrieve the block created from the filter', function(done){
            console.log('Block Filter:', filterEngine.get(blockFilter))
            filterEngine.del(blockFilter)
            done()
        })
    });

    describe('Handling smart contract calls', function(){
        let SM = null;
        let address = null;
        let retrievedValue = null;
        let hash = null;
        let setTx = null;
        let estimatedGas = null;
        let valueSet = 'new value in storage';
        before(function(){
            SM = require('./ressources/SmartContract');
        });
        it('should create the smart contract', function(done){
            esn.addTransaction( SM.makeNewTx(), (err, hash) =>{
                expect(err).to.be.null;
                setTimeout(()=>
                    esn.getTransactionReceipt(hash, (err, result)=>{
                        expect(err).to.be.null;
                        expect(result.contractAddress).to.be.not.null;
                        console.log('Transaction is :', result)
                        result.should.have.property('status', 1);
                        address = result.contractAddress;
                        done();
                    }) , 200 )
            });
        });
        it('should accept gas estimation by playing tx', function(done){
            setTx = SM.makeSetTx(address, valueSet);
            esn.playTransaction( setTx, 'latest', (err, result) => {
                expect(err).to.be.null;
                expect(result).to.not.be.null;
                estimatedGas = result.gasUsed;
                expect(+estimatedGas).to.be.above(21000);
                done();
            });
        });
        it('should accept smart contract transaction', function(done){
            esn.addTransaction( setTx, (err, _hash) =>{
                expect(err).to.be.null;
                setTimeout(()=>esn.getTransactionReceipt(_hash, (err,result)=>{
                    expect(result.contractAddress).to.be.null;
                    result.should.have.property('status', 1);
                    expect(result.gasUsed).equal(estimatedGas);
                    hash = result.transactionHash;
                    console.log(result);
                    done();
                }), 200 )
            });
        });
        it('should enable to read the contract', function(done){
            esn.playTransaction( SM.makeGetTx(address), 'latest', (err, result) =>{
                expect(err).to.be.null;
                expect(result.contractAddress).to.be.null;
                result.should.have.property('status', 1);
                result.should.have.property('return');
                retrievedValue = SM.decodeGetResult(result.return);
                expect(retrievedValue).to.equal(valueSet)
                //console.log('decoded result:', retrievedValue);
                done();
            });
        });
        it('should enable to read storage directly on block 4', function(done){
            esn.getStorageAt(address, 0, 4, (err, value)=>{
                expect(err).to.be.null;
                expect(value.length).to.equal(0);
                //console.log('StorageAt returned', value);
                done();
            });
        });        
        it('should enable to read storage directly on last block', function(done){
            esn.getStorageAt(address, 0, 'latest', (err, value)=>{
                expect(err).to.be.null;
                expect(value.length).not.equal(0);
                value = [...value];
                value = value.slice(0, value[31]/2); // Ok only for string smaller than 31 characters
                //console.log('StorageAt returned:', new Buffer(value).toString());
                expect(new Buffer(value).toString()).equal(valueSet);
                done();
            });
        });
        it('should be able to retrieve a transaction receipt', function(done){
            esn.getTransactionReceipt(hash, (err, receipt) => {
                expect(err).to.be.null;
                expect(receipt).to.have.property('transactionHash', hash)
                expect(receipt).to.have.property('blockNumber', '0x5')
                expect(receipt).to.have.property('from', SM.newWallet.getAddressString())
                expect(receipt).to.have.property('to', address)
                expect(receipt).to.have.property('gasUsed', estimatedGas.toString(16))
                expect(receipt.logs).to.have.property('length', 1)

                //console.log(receipt.logs);
                done();
            });
        });
        it('should fail retrieving from a deleted filter', function(done){
            expect( ()=>filterEngine.get(blockFilter) ).to.throw()
            done()
        })

    });
});
