const Buffer = require('safe-buffer').Buffer // use for Node.js <4.5.0
const async = require('async')
const Trie = require('merkle-patricia-tree/secure')
const Block = require('ethereumjs-block')
const Blockchain = require('ethereumjs-blockchain')
const BlockHeader = require('ethereumjs-block/header.js')
const VM = require('../../lib/ethereum-vm')
const level = require('level')
const levelMem = require('level-mem')
const Account = require('ethereumjs-account').default
const utils = require('ethereumjs-util')
const BN = utils.BN
const rlp = utils.rlp

//const testData = require('./bcRPC_API_Test.json').RPC_API_Test;
// will modify the testData with hashes and stateRoot generated during the process
module.exports = function initBlockchainDB(testData, db, callback) {
  // inMemory blockchainDB
  var blockchainDB = (db? db : levelMem())

  var state = new Trie(blockchainDB)
  //state.checkpoint() // Must not checkpoint (or after each block) otherwise the stateRoots are not valid

  // start with an in-memory blockchain, then after rewrite the full blockchain after the vm processing !!
  var blockchain = new Blockchain(levelMem())
  blockchain.ethash = { verifyPOW: (block, cb)=>{ cb(true) } };
  //blockchain.ethash.cacheDB = level('./.cachedb')
  
  
  function addHexPrefixInHeader(header) {
    header.parentHash = utils.addHexPrefix(header.parentHash);
    header.bloom = utils.addHexPrefix(header.bloom);
    header.coinbase = utils.addHexPrefix(header.coinbase);
    header.hash = utils.addHexPrefix(header.hash);
    header.mixHash = utils.addHexPrefix(header.mixHash);
    header.receiptTrie = utils.addHexPrefix(header.receiptTrie);
    header.nonce = utils.addHexPrefix(header.nonce);
    header.stateRoot = utils.addHexPrefix(header.stateRoot);
    header.transactionsTrie = utils.addHexPrefix(header.transactionsTrie);
    header.uncleHash = utils.addHexPrefix(header.uncleHash);
    return header;
  }
  function prepareBlock(block) {
    let res = {}
    let json = block.toJSON();
    res.blockHeader = {}
    block.header._fields.forEach( (name,index)=>res.blockHeader[name]= json[0][index])
    res.blockHeader.hash= utils.bufferToHex(block.hash());
    res.blocknumber = new BN(block.header.number).toNumber().toString();
    res.rlp = utils.bufferToHex(block.serialize());
    res.transactions = block.transactions.map( (tx, txIndex)=>{
      let otx = {}
      tx._fields.forEach( (name, index)=> otx[name]=json[1][txIndex][index]) 
      return otx
    })
    res.uncleHeaders=[];
    if(block.uncleHeaders.length>0) {
      console.log('Uncles');
    }
    return res;
  }

  var vm = new VM(blockchainDB, {
    state: state,
    blockchain: blockchain
  })
  var genesisBlock = new Block()

  vm.on('beforeTx', function (tx) {
    tx._homestead = true
  })

  vm.on('beforeBlock', function (block) {
    block.header.isHomestead = function () {
      return true
    }
  })

  async.series([
    // set up pre-state
    function (next) {
      //console.log("setup preconditions");
      setupPreConditions(state, testData, next)
    },

    // create and add genesis block
    function (next) {      
      genesisBlock.header = new BlockHeader(
          addHexPrefixInHeader(testData.genesisBlockHeader)
                            )
      genesisBlock.header.stateRoot = state.root;
      blockchain.putGenesis(genesisBlock, next)
    },

    // add some following blocks
    function (next) {
      let blocksByIndex = [genesisBlock];
      //console.log("Adding blocks from the test data")
      async.eachSeries(testData.blocks, eachBlock, next)

      function eachBlock (raw, cb) {
        //console.log("adding block", raw.blockHeader.number);
        try {
          var block = new Block(
              Buffer.from(raw.rlp.slice(2), 'hex'))
          let blockNumber = new BN(block.header.number).toNumber();
          let parentBlock = blocksByIndex[blockNumber-1];
          block.header.parentHash = parentBlock.hash()
          block.uncleHeaders=[]
          block.header.uncleHash= utils.toBuffer('0x1dcc4de8dec75d7aab85b567b6ccd41ad312451b948a7413f0a142fd40d49347')
          //block.header.stateRoot = state.root
          // forces the block into thinking they are homestead
          block.header.isHomestead = function () {
            return true
          }
          block.uncleHeaders.forEach(function (uncle) {
            uncle.isHomestead = function () { return true }
          })

          blockchain.putBlock(block, function (err) {
            //console.log('block added', err, block.header.number, block.hash().toString('hex'), block.header.parentHash.toString('hex'))
            while(blockNumber>=blocksByIndex.length) blocksByIndex.push(null)
            blocksByIndex[blockNumber] = block
            cb(err)
          })
        } catch (err) {
          cb()
        }
      }
    },
    function initPersistedBlockchain(next) {
      testData.generatedBlocks=[];
      blockchain = new Blockchain(blockchainDB);
      blockchain.ethash = { verifyPOW: (block, cb)=>{ cb(true) } };
      blockchain.putGenesis(genesisBlock, next)
    },
    // make sure the blocks check
    // if a block is missing, vm.runBlockchain will fail
    function runBlockchain (next) {
      testData.generatedBlocks.push(prepareBlock(genesisBlock));

      const txLookupPrefix = Buffer.from('l'); // txLookupPrefix + hash -> transaction/receipt lookup metadata
      const txLookupKey = hash => Buffer.concat([txLookupPrefix, hash]);
      function putTxBlockLookup(txHash, blockHash, txIndex, cb) {
        //console.log('putTxBlockLookup', txHash, blockHash, txIndex);
        let dbOps = [
            {
                type: 'put',
                key: txLookupKey(utils.toBuffer(txHash)),
                keyEncoding: 'binary',
                valueEncoding: 'binary',
                value: utils.rlp.encode([ utils.toBuffer(blockHash), txIndex])
            }
        ];
        blockchain._batchDbOps(dbOps, cb);
      }

      vm.runBlockchain((err, results) => {
        //console.log('runBlockchain results:', results.length)
        let currentBlock = null;
        let lastHash = genesisBlock.hash();
        let nextNumber = new BN(1);
        let parentBlock = genesisBlock;
  
        async.eachOfSeries(results, (result, index, cb)=>{
          if(result.length==0) return cb(null)
          
          currentBlock = result[0].block
          currentBlock.header.parentHash=lastHash;
          currentBlock.header.number=nextNumber;
          currentBlock.header.difficulty = currentBlock.header.canonicalDifficulty(parentBlock);
          //console.log('Add Block:', currentBlock.header.parentHash.toString('hex') , currentBlock.hash().toString('hex'))
          
          blockchain.putBlock(currentBlock, (err)=> {
            if(!err) {
              lastHash = currentBlock.hash();
              parentBlock = currentBlock;
              nextNumber.iaddn(1);
    
              async.eachOf(currentBlock.transactions,
                (tx, index, cb) => putTxBlockLookup(tx.hash(), currentBlock.hash(), index, cb), 
                txErrs=>err=txErrs)

              //console.log('done'); 
              testData.generatedBlocks.push(prepareBlock(currentBlock));
            } //else console.log('failure ', err); 
            cb();
          })
  
        }, err=>{
          blockchain.iterator('vm', (block, reorg, cb)=>{
            cb()
          }, next)
        });
      })
    },
/* Was to test and discovered that checkpoint and commit does not allow intermediary stateRoot to be valid
    function testTrie(next) {
      let vm2 = vm.copy();
      vm2.stateManager.setStateRoot(genesisBlock.header.stateRoot, err=>{
        console.log('VM2 set', err)
        next(err)
      })
    },
*/
  ], function (err) {
    console.log('--- Finished processing the BlockChain ---')
    console.log('New head:', '0x' + blockchain.meta.rawHead.toString('hex'))
    //console.log('Expected:', testData.lastblockhash)
    callback(err, {db:blockchainDB, root:state.root});    
  })

  function setupPreConditions (state, testData, done) {
    var keysOfPre = Object.keys(testData.pre)

    async.eachSeries(keysOfPre, function (key, callback) {
      var acctData = testData.pre[key]
      var account = new Account()

      account.nonce = format(acctData.nonce)
      account.balance = format(acctData.balance)

      var codeBuf = Buffer.from(acctData.code.slice(2), 'hex')
      var storageTrie = state.copy()
      storageTrie.root = null

      async.series([

        function (cb2) {
          var keys = Object.keys(acctData.storage)

          async.forEachSeries(keys, function (key, cb3) {
            var val = acctData.storage[key]
            val = rlp.encode(Buffer.from(val.slice(2), 'hex'))
            key = utils.setLength(Buffer.from(key.slice(2), 'hex'), 32)

            storageTrie.put(key, val, cb3)
          }, cb2)
        },
        function (cb2) {
          account.setCode(state, codeBuf, cb2)
        },
        function (cb2) {
          account.stateRoot = storageTrie.root

          if (testData.exec && key === testData.exec.address) {
            testData.root = storageTrie.root
          }
          state.put(Buffer.from(utils.stripHexPrefix(key), 'hex'), account.serialize(), function () {
            cb2()
          })
        }
      ], callback)
    }, done)
  }

  function format (a, toZero, isHex) {
    if (a === '') {
      return Buffer.alloc(0)
    }

    if (a.slice && a.slice(0, 2) === '0x') {
      a = a.slice(2)
      if (a.length % 2) a = '0' + a
      a = Buffer.from(a, 'hex')
    } else if (!isHex) {
      a = Buffer.from(new BN(a).toArray())
    } else {
      if (a.length % 2) a = '0' + a
      a = Buffer.from(a, 'hex')
    }

    if (toZero && a.toString('hex') === '') {
      a = Buffer.from([0])
    }

    return a
  }

}