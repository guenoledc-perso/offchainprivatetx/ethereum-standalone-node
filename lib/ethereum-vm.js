const async = require('async');
const Common = require('ethereumjs-common').default;
const Account = require('ethereumjs-account').default;
const level = require('level');
const Trie = require('merkle-patricia-tree/secure');
const EVM = require('ethereumjs-vm');
const ethUtil = require('ethereumjs-util');
const BN = ethUtil.BN;
const rlp = ethUtil.rlp;

const TransactionReceipts = require('./transaction-receipts');

const lastRootKey = 'fr.yahoo.guenoledc.lastStateTrieRoot';
function saveLastTrieRoot(db, root, cb) { 
    if(db && db.isOpen() )
        db.put(lastRootKey, root.toString('hex'), cb);
    else cb('provide an open level DB instance');
}
// Function to load the last trieRoot saved in the DB
function loadLastTrieRoot(db, cb) {
    if(db)
        db.get(lastRootKey, (err, root) => {
            if(err)
                if( err.notFound ) cb(null, ethUtil.KECCAK256_RLP);
                else cb(err, null);
            else cb(null, new Buffer(root,'hex'));
        });
    else cb('provide an open level DB instance');
}

// Function to save the Transaction Receipt and add properly the trie root into the block
function persistTransactionReceipts(db, blockResults, cb) {

    function persistOneBlockReceipts(blockResult, cb) {
        let tr = TransactionReceipts.fromBlockResult(blockResult);
        if(tr) tr.saveToDb(db, cb);
        else cb() // IMPORTANT - IF MISSED PROCESS DOES NOT STOP !!!
    }

    async.each(blockResults, persistOneBlockReceipts , (err)=>cb(err,blockResults));

}

class EthVM extends EVM {
    constructor(db, opts) {
        if( !db ) throw new Error('You must provide a valid level DB instance');
        let options = opts;
        if(!options.state) options.state = new Trie(db);
        super(options);
        this.db = db;
    }

    setStateRoot(root, cb) {
        let self = this;
        this.stateManager.setStateRoot(root, (err) => {
            if(!err) saveLastTrieRoot(self.db, root, cb)
            else cb(err)
        })
    }

    initState(additionalInit, cb) {
        const self = this;
        
        loadLastTrieRoot(self.db, (err, root) => {
            if(err) return cb(err);
            self.stateManager.setStateRoot(root, (err) => {
                self.stateManager.getStateRoot( (err, root) => {
                    if(err) return cb('No root found!');
                    if( root != self.stateManager._trie.EMPTY_TRIE_ROOT ) return cb();

                    // we have an empty state, initialize the precompiled contracts
                    self.stateManager.checkpoint(()=>{});
                    async.each(Object.keys(this._precompiled), 
                        (p, cb) => {
                            self.stateManager.putAccount(new Buffer(p,'hex'), new Account(), (err) => cb(err));
                        }, 
                        (err) => {
                            if( !err ) self.stateManager.commit(completion);
                            else self.revert( (err2)=>completion(err) ); // sends the original error to the callback
                        });
                });
            });
        });

        function completion(err) {
            if(err) return cb(err);
            if(additionalInit) additionalInit(cb);
            else cb(null);
        }
    }

    displayTransactionResult(txResult) {
        console.log("TransactionReceipt:", txResult.transaction.hash().toString('hex'));
        console.log("----------- status:", txResult.receipt.status);
        console.log("---------- gasUsed:", txResult.result.gasUsed.toString(10));
        if( txResult.result.createdAddress )
            console.log("--- createdAddress:", txResult.result.createdAddress.toString('hex'));
        console.log("logs to be decoded:", txResult.receipt.logs);
        if( txResult.receipt.status==0 ) 
            console.log("--- Failure reason:", txResult.result.exception);
    }
    displayLastRunBlockchainResult(results, cb) {
        if( !results ) return cb('No block results constructed!');
        if( results.length == 0 ) return cb(null); // no block processed
        results.forEach( (txResults) => txResults.forEach( this.displayTransactionResult ));
        cb(null);
    }

    logRoot(when) {
        let root=ethUtil.KECCAK256_RLP_S;
        let isCheckpoint=false;
        if(this.stateManager._trie) {
            root = this.stateManager._trie.root.toString('hex');
            isCheckpoint=this.stateManager._trie.isCheckpoint;
        }
        console.log((when?when:'') + ' - stateTrie.root=', root, 'checkpoint:', isCheckpoint);
    }
}

// replace the runBlockchain by a corrected version
// ATTENTION - This is a copy of the original source with a modification on how the runBlock is called to allow stateRoot of the blockHeader to be set after the commit
EthVM.prototype.runBlockchain = function(cb) {
    let modifiedRunBlockchain = require('./runBlockchain.js').bind(this);
    let self = this;
    let results = null;
    //self.logRoot('START runBlockchain');
    async.seq(
        cb=>modifiedRunBlockchain((err, res) => {
            if(err) {
                // relaunch the Run because blocks may have been removed. If not, the call will do nothing
                modifiedRunBlockchain( (e, r)=>cb(err))
            } else {
                results = res; // collect the processing results
                cb(err)
            }
        }),
        // Save the transaction's receipt and update the block's receiptTrie
        cb=>persistTransactionReceipts(self.db, results, cb),
        (blockResults, cb)=>{results=blockResults, cb(null)},
        // save in the db the last state root of the merkle trie
        cb=>saveLastTrieRoot(self.db, self.stateManager._trie.root, cb)
    )( err=>cb(err, results) );

}



module.exports = EthVM;