/**
 * Meant to implement Filters on node side to query the blocks
 * over the Ethereum Standalone node implemented above EthereumJS modules
 * @Author guenoledc@yahoo.fr
 */ 
const EthereumStandaloneNode = require('../../index')
const TransactionReceipts = require('../transaction-receipts')
const EthUtils = require('ethereumjs-util')
const BN = EthUtils.BN

const BLOCK_TYPE = 'block'
const LOG_TYPE = 'log'
class FilterEngine {
    constructor(obsoleteTimer){
        this.filters = {}
        this.counter = 0;
        if( obsoleteTimer && obsoleteTimer>0 ) setInterval( removeObsoleteFilter.bind(this), obsoleteTimer )
    }
    newFilter(esn, type, data, loadHistory) {
        //console.log('esn instanceof', esn.__proto__)
        // below does not work when the esn instance has been created outside this package ????
        //if(!(esn instanceof EthereumStandaloneNode)) throw new TypeError('Expecting a valid FilterEngine')
        if(type==BLOCK_TYPE) return new BlockFilter(esn)
        if(type==LOG_TYPE) return new LogFilter(esn, data, loadHistory)
        throw new TypeError('Not implemented filter type', type)
    }
    add(filter) {
        let id = EthUtils.bufferToHex(this.counter)
        this.filters[id] = {filter:filter, id:id, created:Date.now()} // id and created are not used for now --> remove?
        this.counter++
        return id
    }
    del(id) {
        if(id in this.filters) {
            // unregister the event listener
            this.filters[id].filter.clear()
            delete this.filters[id];
            return true
        } 
        return false
    }
    get(id) {
        if(id in this.filters) {
            return this.filters[id].filter.getFromLastCall()
        } else throw new TypeError('the filter id does not exists - it may have been made obsolete due to innactivity')
    }
    getAll(id, cb) {
        if(id in this.filters) {
            return this.filters[id].filter.getAll(cb)
        } else throw new TypeError('the filter id does not exists - it may have been made obsolete due to innactivity')
    }
    getAllWithOptions(esn, data, cb) {
        this.newFilter(esn, LOG_TYPE, data, false).getAll(cb)
    }
}
function removeObsoleteFilter() {
    let self=this;
    Object.keys(self.filters)
        .filter( id=>self.filters[id].filter.isObsolete() )
        .forEach( id=>self.del(id) )
}

class Filter {
    constructor(esn, type) {
        this.esn = esn
        this.touch()
    }
    clear() {throw new Error('Not Implemented')}
    touch() {this.touched = Date.now()}
    isObsolete() { Date.now()-this.touched > 5*60*1000 } // after 5 minutes inactivity the filter can be deleted

    getFromLastCall() {throw new Error('Not Implemented')}
    getAll(cb) {throw new Error('Not Implemented')}
}


// must be called with a binding to an instance of Filter
function processBlock(block) {
    this.touch()
    this.blocks.push(EthUtils.bufferToHex(block.hash()))
}

class BlockFilter extends Filter {
    constructor(esn) {
        super(esn, BLOCK_TYPE);
        this.handler = processBlock.bind(this)
        this.esn.on('block', this.handler )
        this.blocks = [] // array of blocks received
    }
    clear() {
        this.esn.removeListener('block', this.handler )
    }
    getFromLastCall() {
        this.touch()
        let ret = this.blocks
        this.blocks = []
        return ret
    }
    getAll(cb) {
        throw new Error('This filter is a BlockFilter and this method does not apply')
    }
}
const inValue = (test, into) => {
    if(into==null) return true
    if(!Array.isArray(into)) into=[into]
    return into.includes(test)
}

class LogFilter extends Filter {
    constructor(esn, options, loadHistory) {
        if(!options) options = {}
        if(typeof options !== 'object') throw new TypeError('Expecting an options object')
        if(options.blockhash) { 
            delete options.fromBlock
            delete options.toBlock 
        } else {
            delete options.blockhash
            if(options.fromBlock === undefined) options.fromBlock='latest'
            if(options.toBlock === undefined) options.toBlock='latest'
            // if from latest, to is latest as well regardless of the config - its an implementation choice - not in specification
            if(options.fromBlock == 'latest' ) options.toBlock='latest'
        }
        if(options.address === undefined) options.address=null
        if(options.topics === undefined) options.topics=[]

        super(esn, LOG_TYPE);
        this.handler = this.addLog.bind(this)
        // subscribe to new events only. For historical event a db search must be executed
        this.esn.on('log', this.handler )
        this.logs = [] // array of blocks received and matching
        this.options = options
        if(loadHistory) this.searchHistory()
    }
    filterLog(log) {
        // here, filter on address and topics for each incoming logs
        /**
         * this.options follows the structure at https://github.com/ethereum/wiki/wiki/JSON-RPC#parameters-38
         * But all parameters are set to default value 
         * .fromBlock 'latest' or BN
         * .toBlock 'latest' or BN
         * .blockhash 
         * .address is null if no filter, [] for accepting no address, [add1, add2, ...] for only accepting these addresses
         * .topics is [] for no filter, else each item should match the position in log.topics in the form of
         *      null for no filter, a single value to match, an array of accepted values
         * 
         * log follows the structure at https://github.com/ethereum/wiki/wiki/JSON-RPC#returns-42 for logs
         *  
         */
        let self = this
        if(self.options.blockhash) 
            if(self.options.blockhash != log.blockHash) return null // expecting the correct block hash
            else ; // continue checking conditions
        else { // there are block number filtering
            let infos = self.esn.getBlockchainInfo()
            let bnLogBlock = new BN(EthUtils.toBuffer(log.blockNumber))
            let fromBlock, toBlock
            if(self.options.fromBlock=='latest') fromBlock=infos.blockNumber // this is a BN
            else fromBlock = new BN( EthUtils.toBuffer(self.options.fromBlock))
            if(self.options.toBlock=='latest') toBlock=infos.blockNumber // this is a BN
            else toBlock = new BN( EthUtils.toBuffer(self.options.toBlock))
            // reject if the log is in block before or after the [from, to] segment
            if(bnLogBlock.lt(fromBlock) || bnLogBlock.gt(toBlock)) return null
        }
        // test the address is in the given values => if not do not integrate the log
        if(!inValue(log.address, self.options.address)) return null;
        // for all given positions in the option topics array check if the received topic match
        for (let i = 0; i<self.options.topics.length; i++) {
            // first if there is no log topic at that position log.topics[i] = undefined and it will only be accepted if the option is null
            if( !inValue(log.topics[i], self.options.topics[i]) ) return null;
        }
        // all tests have passed, return the log
        return log
    }
    addLog(log) {
        if(log) this.logs.push(log)
    }
    searchHistory(logHandler, onCompleted) {
        let self = this
        if(!logHandler) logHandler=this.handler
        if(!onCompleted) onCompleted= ()=>{}

        function processBlock(block, cb) {
            TransactionReceipts.loadFromDb(self.esn.db, block, (err, tr)=>{
                if(err) return; // ignore error
                tr.logsIteratorSync( log=>{if(self.filterLog(log)) logHandler(log) })
                cb(null)
            })
        }
        if(self.options.blockhash) {
            self.esn.blockchain.blockchain.getBlock( EthUtils.toBuffer(self.options.blockhash), (err,block)=>{
                if(err) {
                    console.error('Failure in getting block by hash ', self.options.blockhash, ':', err)
                    try { onCompleted(null) } catch (failCallingCompletion) { }
                } else processBlock(block, ()=>{
                    try { onCompleted(null) } catch (failCallingCompletion) { }
                })
            });
        } else { // there are block number specified
            let infos = self.esn.getBlockchainInfo()
            let fromBlock, toBlock
            if(self.options.fromBlock=='latest') fromBlock=infos.blockNumber // this is a BN
            else fromBlock = new BN( EthUtils.toBuffer(self.options.fromBlock))
            if(self.options.toBlock=='latest') toBlock=infos.blockNumber // this is a BN
            else toBlock = new BN( EthUtils.toBuffer(self.options.toBlock))
            // check the order
            if(toBlock.lt(fromBlock)) toBlock=fromBlock
            self.esn.blockIterator(fromBlock, toBlock, processBlock, 
                (err, lastBlock)=>{
                // TODO: I cannot see if it is a problem to have an error here. It means that the infos.blockNumber has been set to the new block number before being actually written in the DB
                if(err) console.error('Failure in iterating block at ', EthUtils.bufferToHex(lastBlock), ':', err)
                self.lastBlock=lastBlock
                try { onCompleted(err) } catch (failCallingCompletion) { }
            })
        }
    }
    clear() {
        this.esn.removeListener('log', this.handler )
    }
    getFromLastCall() {
        this.touch()
        let ret = this.logs
        this.logs = []
        return ret
    }
    getAll(cb) {
        if(typeof cb !== 'function') throw new TypeError('cannot use filter.getAll without a callback(err,log[])')
        let localLogs = []
        this.searchHistory(log=>localLogs.push(log),
            err=>{ // on completion feed the callback
                try{cb(err, localLogs)}catch(failCallingCallback){}
            }
        )
    }
}


module.exports=FilterEngine
module.exports.BLOCK_TYPE = BLOCK_TYPE
module.exports.LOG_TYPE = LOG_TYPE
