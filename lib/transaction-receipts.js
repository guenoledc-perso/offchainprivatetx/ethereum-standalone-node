const async = require('async');
const Trie = require('merkle-patricia-tree/secure');
const EthUtils = require('ethereumjs-util');
const BN = EthUtils.BN;
const rlp = EthUtils.rlp;

class TransactionReceipts {
    // the block result is the result created by runBlockchain 
    constructor(block, raw) {
        if(!block) throw new TypeError('ensure the instance is initiated with a valid block');
        if(!Array.isArray(raw)) throw new TypeError('the raw parameter must be an array of receipt array');

        this.raw = raw; // this is 
        this.block = block; // the block to which the receipt belongs to
    }

    static fromBlockResult(blockResult) {
        function collectReceipt(txResult) {
            let rawReceipt = [];
            let exception = txResult.result.exception
            if( exception && exception.error ) exception=exception.error // we may have a VMError instance
            rawReceipt.push(txResult.result.gasUsed.toBuffer()); // 0-gasUsed
            rawReceipt.push(txResult.receipt.gasUsed); // 1-cumulativeGasUsed
            rawReceipt.push(txResult.result.createdAddress?txResult.result.createdAddress:null); // 2-contractAddress
            rawReceipt.push(txResult.receipt.rawLogs); // 3-logs
            rawReceipt.push(txResult.result.bloom.bitvector); // 4-logsBloom
            rawReceipt.push(txResult.receipt.status); // 5-status
            rawReceipt.push(txResult.result.result); // 6-return
            rawReceipt.push(exception); // 7-exception from vm or null
            return rawReceipt;                
        }
        function collectReceipts(blockResult) {
            let rawReceipts=[]; // an array of, per transaction, an array of fields
            rawReceipts = blockResult.map(collectReceipt);
    
            return rawReceipts;
        }
        if(blockResult.length==0) return null;
        return new TransactionReceipts(blockResult[0].block, collectReceipts(blockResult));
    }

    static loadFromDb(db, block, cb) {
        if(!db) throw new TypeError('expecting a valid LevelDB instance to load the receipts from');
        if(!block) throw new TypeError('ensure the instance is initiated with a valid block');
        if(!block.header) throw new TypeError('expecting a header instance in the block');
        if(!block.header.receiptTrie) throw new TypeError('expecting to have a receiptTrie in the block header');
        // add control on what type the receiptTrie should be
        //console.log('Recp trie root:', block.header.receiptTrie.toString('hex'))
        let trie = new Trie(db, block.header.receiptTrie);
        let index=0;
        let found=true;
        let raw = [];
        async.doWhilst(
            (cb)=>trie.get(rlp.encode(index), (err, value)=>{
                //console.log('LoadFromdb:', index, value, err)
                if(err || !value) found=false;
                else raw.push(rlp.decode(value));
                index++; 
                cb(err);
            }),
            ()=>found,
            (err)=> {// pass the error and the new instance to the caller
                if(err) cb(err, null);
                else cb(null, new TransactionReceipts(block, raw)); 
            }
        );
    }

    saveToDb(db, cb) {
        if(typeof cb !== 'function') cb = ()=>{};
        if(!db) throw new TypeError('expecting a valid LevelDB instance to save the receipts');
        if(!this.block) throw new Error('ensure the instance is initiated with a valid block');
        let self=this;

        let trie = new Trie(db);
        async.eachOfSeries(this.raw, 
            (rawReceipt, index, cb)=>{
                try{
                    //console.log('Saving tx Receipt', index, this.block.hash().toString('hex'))
                    trie.put(rlp.encode(index), rlp.encode(rawReceipt), cb);
                }catch(failed){
                    console.log('fail processing receipt', rawReceipt)
                    return cb(failed);
                }    
            }, (err)=>{
                if(err) cb(err)
                else {
                    self.block.header.receiptTrie = new Buffer(trie.root);
                    //console.log('Rcp Trie root:', self.block.header.receiptTrie.toString('hex'))
                    cb(null);
                }
            });
    
    }
    static formatRawLogs(block, transaction, txIndex, logs) {
        // follows specification https://github.com/ethereum/wiki/wiki/JSON-RPC#returns-42
        if(!logs || !Array.isArray(logs) ) return [];
        return logs.map( (log, index) => { 
            return TransactionReceipts.formatRawLog(block, transaction, txIndex, log, index)
        } )
        
    }
    static formatRawLog(block, transaction, txIndex, log, index) {
        return {
            removed: false,
            logIndex: '0x'+new BN(index).toString(),
            transactionIndex: '0x'+new BN(txIndex).toString(),
            transactionHash: EthUtils.bufferToHex(transaction.hash()),
            blockHash: EthUtils.bufferToHex(block.hash()),
            blockNumber: '0x'+new BN(block.header.number).toString(16),
            address: EthUtils.bufferToHex(log[0]),
            data: EthUtils.bufferToHex(log[2]),
            topics: log[1].map( topic=> EthUtils.bufferToHex(topic) )
        }         
    }
    _formatTransactionReceipt(index) {
        // create a response respecting the format in https://github.com/ethereum/wiki/wiki/JSON-RPC#returns-31
        let self = this;
        let raw = self.raw[index];
        let transaction = self.block.transactions[index];
        return {
            transactionHash: EthUtils.bufferToHex(transaction.hash()),
            transactionIndex: index,
            blockHash: EthUtils.bufferToHex(self.block.hash()),
            blockNumber: '0x'+new BN(self.block.header.number).toString(16),
            from: EthUtils.bufferToHex(transaction.from),
            to: EthUtils.bufferToHex(transaction.to),
            gasUsed: '0x'+new BN(raw[0]).toString(16), // to be tested on multiple tx 
            cumulativeGasUsed: '0x'+new BN(raw[1]).toString(16), // to be tested on multiple tx 
            contractAddress: (raw[2] && raw[2].length>0?EthUtils.bufferToHex(raw[2]):null),
            logs: TransactionReceipts.formatRawLogs(self.block, transaction, index, raw[3]),
            logsBloom: EthUtils.bufferToHex(raw[4]),
            status: new BN(raw[5]).toNumber(),
            'return': EthUtils.bufferToHex(raw[6]),
            exception: (raw[7]?raw[7].toString('utf8'):'')
        };
    }

    toWeb3Format(index) {
        if(index<0 || index>=this.raw.length) throw new TypeError('expecting a receipt index in the range 0:'+this.raw.length-1);
        if(index<0 || index>=this.block.transactions.length) throw new TypeError('expecting a transaction index in the range 0:'+this.block.transactions.length-1);
        
        return this._formatTransactionReceipt(index);
    }
    logsIteratorSync(onLog) {
        if(typeof onLog !== 'function') return
        let self = this;
        this.raw.forEach( (rawTxReceipt, index)=>{
            let transaction = self.block.transactions[index];
            let logs = rawTxReceipt[3];
            logs.forEach( (rawLog, logIndex)=>{
                try {onLog(TransactionReceipts.formatRawLog(self.block,transaction, index, rawLog, logIndex))}catch(failCallingOnLog){}
            })
        })
    }
}

module.exports=TransactionReceipts;