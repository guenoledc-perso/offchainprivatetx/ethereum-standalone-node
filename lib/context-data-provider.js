/**
 * Provides elements to various part of the code (new block creation, receipts ...)
 * It is created to have a hook to the highest context (either the simple standalone node or as the case may be 
 * an higher node - eg EthereumPrivacy)
 * In case some different implementation is needed, the implemented should respect the interface and eventually inherit
 */


const timestamp = () =>  Math.floor(Date.now() / 1000);

class ContextDataProvider {
    getTimestamp() {
        return timestamp()
    }
}

module.exports = ContextDataProvider;