/**
 * Meant to handle wallets and keep them safe
 * over the Ethereum Standalone node implemented above EthereumJS modules
 * @Author guenoledc@yahoo.fr
 */

const Wallet = require('ethereumjs-wallet')
const Transaction = require('ethereumjs-tx')
const EthUtil = require('ethereumjs-util')
const fs = require('fs')


function buildKeystoreLocation(location) {
    if(!location || typeof location !=='string' || location=='memory') return null;
    if( fs.existsSync(location) ) return fs.realpathSync(location);
    
    try {
        fs.mkdirSync(location);
        return fs.realpathSync(location);
    } catch (failMkDir) {
        console.error('cannot create the keystore file:', failMkDir);
        return null;
    }
}

function scanWalletsInLocation(location, existingWallets) {
    if(!location) return existingWallets;
    if(!fs.existsSync(location)) return existingWallets;
    let newWallets={};
    
    fs.readdirSync(location).filter(file=>file.endsWith('.json')).forEach(file=>{
        let wallet = JSON.parse( fs.readFileSync(location+'/'+file) );
        newWallets['0x'+wallet.address] = {wallet:wallet, locked:true, file:file}
    })
    // delete wallets not in the keystore

    Object.keys(existingWallets).filter(address=>!(address in newWallets)).forEach(address=>{
        if(existingWallets[address].locked==true) {
            console.log('Removing wallet', address)
            delete existingWallets[address]
        // if unlocked, the private address remains until it is locked again
        }
    })
    // add new wallets without overriding their status
    Object.keys(newWallets).forEach(address=>{
        if(address in existingWallets) {
            existingWallets[address].wallet=newWallets[address].wallet
            existingWallets[address].file=newWallets[address].file
        } else {
            existingWallets[address]=newWallets[address]
            console.log('Discovered a new wallet', existingWallets[address].file)
        }
    })
    return existingWallets;
}

function deleteWalletFromLocation(location, wallet) {
    if(!location) return false;
    if(!fs.existsSync(location)) return false;
    if(wallet && wallet.wallet && wallet.file) {
        console.log('remove wallet', wallet.file)
        fs.unlinkSync(location+'/'+wallet.file)
    }
    return true;
}
function getV3Filename(address) {
    /*
     * We want a timestamp like 2016-03-15T17-11-33.007598288Z. Date formatting
     * is a pain in Javascript, everbody knows that. We could use moment.js,
     * but decide to do it manually in order to save space.
     *
     * toJSON() returns a pretty close version, so let's use it. It is not UTC though,
     * but does it really matter?
     *
     * Alternative manual way with padding and Date fields: http://stackoverflow.com/a/7244288/4964819
     *
     */
    var ts = new Date();

    return ['UTC--', ts.toJSON().replace(/:/g, '-'), '--', EthUtil.stripHexPrefix(address),'.json'].join('');
};
function saveWalletInLocation(location, address, wallet) {
    console.log('save wallet', address)
    if(!location) return false;
    if(!fs.existsSync(location)) return false;
    if(wallet && wallet.wallet && !wallet.file) {
        wallet.file=getV3Filename(address)
        fs.writeFileSync(location+'/'+wallet.file, JSON.stringify(wallet.wallet,null,2))
    }
    return true;
}

class Keystore {
    constructor(location, scanLocationPeriodMs) {
        // this.location can be null, it means memory only     
        this.location = buildKeystoreLocation(location);

        this._wallets = scanWalletsInLocation(this.location, {});
        this.wallets = new Proxy(this._wallets, {
            get: (obj,prop) => {
                // TODO Do not retrieve sensitive info
                return prop in obj?obj[prop]:{address:null}
            },
            set: (obj, prop, value) => {
                if(prop in obj) 
                    if(value) {
                        obj[prop].wallet=value.wallet
                        obj[prop].locked=value.locked
                    } else {
                        deleteWalletFromLocation(this.location, obj[prop]);
                        delete obj[prop];
                    }
                else if(value) {
                    saveWalletInLocation(this.location, prop, value);
                    obj[prop]=value
                } else return false // should not be allowed to set null value to an unexistant key
                return true;
            }
        })
        let _scanInterval = null;
        let self=this;
        if(this.location && scanLocationPeriodMs 
            && Number.isInteger(scanLocationPeriodMs) 
            && scanLocationPeriodMs>0 )
            self._scanInterval=setInterval(()=>scanWalletsInLocation(self.location, self._wallets), scanLocationPeriodMs)
    }
    hasAccount(address) {
        if( !EthUtil.isValidAddress(address) ) return false
        return (address in this._wallets)  
    }
    newAccount(passphrase) {
        let wallet = Wallet.generate();
        wallet = wallet.toV3(passphrase || '');
        this.wallets[ EthUtil.addHexPrefix(wallet.address)]={wallet:wallet, locked:true};
        return EthUtil.addHexPrefix(wallet.address);
    }
    deleteAccount(address) {
        if( !EthUtil.isValidAddress(address) ) return
        if( !(address in this._wallets) ) return
        this.wallets[address]=null
    }
    newAccountFromPrivateKey(privateKey, passphrase) {
        if( typeof privateKey === 'string' )
            privateKey = EthUtil.toBuffer(privateKey)
        if( !EthUtil.isValidPrivate(privateKey) )
            throw new TypeError('expecting a valid private address in hex string format or buffer') 
        let wallet = Wallet.fromPrivateKey(privateKey)
        wallet = wallet.toV3(passphrase || '');
        this.wallets[ EthUtil.addHexPrefix(wallet.address)]={wallet:wallet, locked:true};
        return EthUtil.addHexPrefix(wallet.address);
    }
    listAccounts() {
        //Object.keys(this._wallets).forEach(address=>console.log(this.wallets[address]))
        return Object.keys(this._wallets)
    }
    unlockAccount(address, passphrase, duration) {
        //console.log('Unlock account', address, duration)
        if( !EthUtil.isValidAddress(address) ) throw new TypeError('expecting a valid hex string address')
        if( !(address in this._wallets) ) throw new TypeError('the wallet with address '+ address+ ' cannot be found in the keystore')
        if( typeof passphrase !== 'string' ) throw new TypeError('expecting a string passphrase')
        if( typeof duration==='undefined' || !Number.isInteger(duration) ) duration=300 
        if( duration < 0 ) duration=-duration

        let wallet=Wallet.fromV3( this.wallets[address].wallet, passphrase );
        this.wallets[address].locked=false;
        this.wallets[address].privateKey=wallet.getPrivateKey();
        let self=this;

        //console.log('Timing b4', Date.now(), this.wallets[address].shouldEnds)
        this.wallets[address].shouldEnds = Math.max( Date.now()+duration , this.wallets[address].shouldEnds||0 )
        if(duration==0) this.wallets[address].shouldEnds = Number.MAX_VALUE

        if(this.wallets[address].timeout) clearTimeout(this.wallets[address].timeout);
        const delay=this.wallets[address].shouldEnds - Date.now()
        if(delay>0 && this.wallets[address].shouldEnds<Number.MAX_VALUE ) 
            this.wallets[address].timeout=setTimeout( ()=>self.lockAccount(address), delay )
        else this.wallets[address].timeout=null
        //console.log('Timing af', Date.now(), this.wallets[address].shouldEnds, delay)
        
    }
    lockAccount(address) {
        console.log('Lock Account', address)
        if( !EthUtil.isValidAddress(address) ) return
        if( !(address in this._wallets) ) return
        this.wallets[address].locked=true;
        delete this.wallets[address].privateKey
        if(this.wallets[address].timeout) clearTimeout(this.wallets[address].timeout)
        delete this.wallets[address].timeout
        delete this.wallets[address].shouldEnds
    }
    ecSignMessage(message, chainId, address, passphrase) {
        if( !(address in this._wallets) ) throw new TypeError('the wallet with address '+ address+ ' cannot be found in the keystore')
        if(!message) message=''
        if(!passphrase && this.wallets[address].locked ) throw new Error('the wallet must be unlocked first. Provide a passphrase')
        
        if(passphrase) this.unlockAccount(address, passphrase, 50);

        return EthUtil.ecsign( EthUtil.hashPersonalMessage(new Buffer(message)), 
                this.wallets[address].privateKey, chainId)
    }
    // corrects the EthUtils version that does not support the ChainId param
    fromRpcSig(sig) {
        sig = EthUtil.toBuffer(sig);
      
        // NOTE: with potential introduction of chainId this might need to be updated
        if (sig.length !== 65 && sig.length !== 66) {
          throw new Error('Invalid signature length');
        }
      
        var v = sig[64];
        if(sig.length==66) v=v*256+sig[65]
        // support both versions of `eth_sign` responses
        if (v < 27) {
          v += 27;
        }
      
        return {
          v: v,
          r: sig.slice(0, 32),
          s: sig.slice(32, 64)
        };
    };
    ecRecover(message, signature, chainId) {
        if(!signature || !signature.r || !signature.s || !signature.v) throw new TypeError('the signature is not a valid {r,s,v} structure')
        if(!message) message=''
        return EthUtil.ecrecover( EthUtil.hashPersonalMessage(new Buffer(message)),
                        signature.v, signature.r, signature.s,
                        chainId)
    }
    signTransaction(address, tx, passphrase) {
        if( !(address in this._wallets) ) throw new TypeError('the wallet with address '+ address+ ' cannot be found in the keystore')
        if( !tx instanceof Transaction ) throw new TypeError('expecting a valid Transaction instance')
        
        if(!passphrase && this.wallets[address].locked ) throw new Error('the wallet must be unlocked first. Provide a passphrase')
        if(passphrase) this.unlockAccount(address, passphrase, 50);

        tx.sign(this.wallets[address].privateKey);
        return tx;
    }
    // update the transaction and return the address used to sign. No Ether in this address
    signTransactionWithAnonymousAccount(tx) {
        if( !tx instanceof Transaction ) throw new TypeError('expecting a valid Transaction instance')
        let wallet = Wallet.generate();
        tx.sign(wallet.getPrivateKey());
        return wallet.getAddressString();
    }
    isLocked(address) {
        if( !(address in this._wallets) ) throw new TypeError('the wallet with address '+ address+ ' cannot be found in the keystore')
        return this.wallets[address].locked 
    }
    stop(cb) {
        if(this._scanInterval)
            clearInterval(this._scanInterval)
        setImmediate(cb)
    }
}
module.exports = Keystore

// USAGE SAMPLE
/*
let keystore = new Keystore('.keystore', 0);
keystore.newAccount('h4ck3r')
keystore.newAccountFromPrivateKey(EthUtil.toBuffer('0xceee921f6252f389fe940329b3c563fb1dbedf6bbd8cf31cb694274916b35bd6'), 'h4ck3r')
console.log('Accounts:', keystore.listAccounts())
keystore.unlockAccount( keystore.listAccounts()[0], 'h4ck3r', 4000)
let sig = keystore.ecSignMessage('Some text to sign', 1234, keystore.listAccounts()[1], 'h4ck3r')
let bkupsig = sig;
sig = EthUtil.toRpcSig(sig.v, sig.r, sig.s, 1234)
console.log('Signed:', sig)
keystore.lockAccount(keystore.listAccounts()[0])
let rec = keystore.ecRecover('Some text to sign', keystore.fromRpcSig(sig), 1234)
console.log('Recovered:', EthUtil.pubToAddress(rec))

let tx = new Transaction({
    gasPrice: '0x1000',
    gasLimit: 100000, //constants.gasLimit,
    chainId : 123,
    nonce: 0, // txcount of the sender's address. Must be consistent with what we put in the trie
    value: '0x1000', // amount of wei to be transferred with the Tx
    to: keystore.listAccounts()[1], // target wallet or contract address, not used for contract creation
    data: '0x'
})
//console.log(keystore.signTransaction(keystore.listAccounts()[0], tx, 'h4ck3r'))

keystore.deleteAccount(keystore.listAccounts()[0])
*/