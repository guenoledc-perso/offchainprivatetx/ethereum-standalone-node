const fs = require('fs')

module.exports.cleanLevelDbSync = (path)=>{
    
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).forEach( file=> fs.unlinkSync(path+'/'+file) ); 
        fs.rmdirSync(path);
    }

}

module.exports.cleanKeystoreSync = (path)=> {
       
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).filter(f=>f.endsWith('.json')).forEach( file=> fs.unlinkSync(path+'/'+file) ); 
        //fs.rmdirSync(path);
    }
 
}