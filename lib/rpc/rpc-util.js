
/**
 * Meant to implement https://github.com/ethereum/wiki/wiki/JSON-RPC
 * over the Ethereum Standalone node implemented above EthereumJS modules
 * @Author guenoledc@yahoo.fr
 */
const async = require('async')
const Transaction = require('ethereumjs-tx')
const EthUtils = require('ethereumjs-util');
const BN = EthUtils.BN;

const PARSE_ERROR=-32700;
const INVALID_REQUEST=-32600;
const METHOD_NOT_FOUND=-32601;
const INVALID_PARAMS=-32602;
const INTERNAL_ERROR=-32603;

class RPCError extends Error {
    constructor(message, rethrown, code) {
        super(message);
        this.code=code||INTERNAL_ERROR;
        this.data=(rethrown?{stack:rethrown.stack, name:rethrown.name}:null);
    }
    toObject() {
        return {message:this.message, code:this.code, data:this.data};
    }
}
class RPCResponse {
    constructor(req, error, result) {
        if(!req) req={};
        this.format=req.format || 'string';
        this.jsonrpc='2.0';
        this.id=req.id;
        
        if(!error) this.error=null;
        else if( error instanceof RPCError) this.error=error;
        else if( error instanceof Error) this.error=new RPCError(error.message,error,INTERNAL_ERROR);
        else if( error.message!==undefined && error.code!==undefined ) this.error=new RPCError(error.message, error.data, error.code)
        else this.error=new RPCError(error, null, INTERNAL_ERROR);

        this.result=result;
    }
    toFormat(format) {
        let out;
        if(this.error) out={jsonrpc:this.jsonrpc, id:this.id, error:this.error.toObject()};
        else out={jsonrpc:this.jsonrpc, id:this.id, result:this.result};
        
        if(format=='string' || this.format=='string')
            return JSON.stringify(out);
        else return out;
    }
}
// expected is an array of expected params each as an object {name:string, default:val, format=['Address'|'BlockTag'|'Bool'|'Quantity']}
// it returns an object with each params recognized on its name and throw an error if any param is missing
function assertParams(params, expected) {
    if(!params || !Array.isArray(params) ) throw new RPCError('Expects a params array of parameters', null, INVALID_PARAMS);
    // get the number of mandatory fields
    let nbExpected = expected.length
    while( nbExpected>0 && expected[nbExpected-1].default!==undefined ) nbExpected--; // because the field is optiona with a default value
    if(params.length<nbExpected) throw new RPCError('Expecting '+nbExpected+' params and only '+params.length+' found', null, INVALID_PARAMS); 
    let res = expected.reduce( (res, def, index) =>{
        //console.log('reduce:', res, def, index)
        let param = params[index];
        if( param===undefined )
            if( def.default !== undefined ) param=def.default;
            else throw new RPCError('Expects a non null parameter at position '+index, null, INVALID_PARAMS);
        if(param !== null)
            switch (def.format) {
                case 'Address':
                    if( !EthUtils.isValidAddress(param) ) throw new RPCError('Expects an address param at position '+index, null, INVALID_PARAMS);
                    break;
                case 'BlockTag':
                    if(['latest', 'earliest', 'pending'].includes(param)) break;
                    if( typeof param === 'number' )
                        param=new BN(param);
                    else if( EthUtils.isHexString(param))
                        param=new BN(EthUtils.toBuffer(param).slice(0,8)); 
                    else throw new RPCError('Expects a valid block tag or number. Found: '+param, null, INVALID_PARAMS);
                    break;
                case 'Bool':
                    param = !!param
                    break;
                case 'Data32':
                    if( !EthUtils.isValidHash256(param) ) throw new RPCError('Expects a hash 256 bits param at position '+index, null, INVALID_PARAMS);
                    break;
                case 'Quantity':
                    if( typeof param === 'number') 
                        param=new BN(param)
                    else if( EthUtils.isHexString(param) ) 
                        param=new BN(EthUtils.toBuffer(param));
                    else throw new RPCError('Expects a valid number. Found: '+param, null, INVALID_PARAMS);
                    break;
                case 'UTF8':
                case 'Data':
                    if(typeof param !== 'string') throw new RPCError('Expects a UTF8 string at position '+index, null, INVALID_PARAMS);
                    param = EthUtils.toBuffer(param);
                    break;
                case 'Object':
                    if(typeof param !== 'object' || Array.isArray(param) ) throw new RPCError('Expects a JSON object at position '+index, null, INVALID_PARAMS);
                    break;
                case 'Array':
                    if( !Array.isArray(param) ) throw new RPCError('Expects a JSON array at position '+index, null, INVALID_PARAMS);
                    break;
            default:
                throw new RPCError('Invalid format in coding', null, INTERNAL_ERROR); 
            }
        res[def.name] = param;
        return res;       
    }, {})
    return res;
}

function prepareNewTransaction(txData, blockNumber, esn, cb) {
    async.seq( // Use of sequence because there might be other async actions to perform
        cb=>{
            if(!txData.nonce && txData) {// nonce not provided get it from the account
                esn.proposeNextNonce(txData.from, blockNumber, (err, nonce) => {
                    txData.nonce = nonce
                    cb()
                });
            } else cb()
        }
    )( err=>cb(err, err?null:new Transaction(txData) ))
}

let makeRpc_reqId=0

function makeRpc(method, params) {
    return { id: makeRpc_reqId++, jsonrpc: '2.0', method: method, params: params || [] }
}

module.exports = {
    PARSE_ERROR: PARSE_ERROR,
    INVALID_REQUEST: INVALID_REQUEST,
    METHOD_NOT_FOUND: METHOD_NOT_FOUND,
    INVALID_PARAMS: INVALID_PARAMS,
    INTERNAL_ERROR: INTERNAL_ERROR,
    RPCError: RPCError,
    RPCResponse: RPCResponse,
    assertParams: assertParams,
    prepareNewTransaction:prepareNewTransaction,
    makeRpc: makeRpc
}
