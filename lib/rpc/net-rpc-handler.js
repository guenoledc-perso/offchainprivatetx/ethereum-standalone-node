/**
 * Meant to implement https://github.com/ethereum/wiki/wiki/JSON-RPC
 * over the Ethereum Standalone node implemented above EthereumJS modules
 * @Author guenoledc@yahoo.fr
 */
const EthUtils = require('ethereumjs-util');
const RpcUtil = require('./rpc-util');
const RPCError = RpcUtil.RPCError;

const ESN = require('../..');

module.exports = function handle_net(req, callback) {



    switch (req.method.split('_')[1]) {
        case 'version':
            let infos = this.getBlockchainInfo();
            callback(null, EthUtils.addHexPrefix(new Number(infos.version).toString(16)));
            break;
        case 'listening':
            callback(null, false);
            break;
        case 'peerCount':
            callback(null, '0x00');
            break;
        default:
            callback(new RPCError('unknown net method '+req.method, null, RpcUtil.METHOD_NOT_FOUND), null);
            break;
    }
}
