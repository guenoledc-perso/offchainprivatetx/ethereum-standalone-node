/**
 * Meant to implement https://github.com/ethereum/wiki/wiki/JSON-RPC
 * over the Ethereum Standalone node implemented above EthereumJS modules
 * @Author guenoledc@yahoo.fr
 */      
const async = require('async')
const RpcUtil = require('./rpc-util');
const RPCError = RpcUtil.RPCError;
const RPCResponse = RpcUtil.RPCResponse;
const PARSE_ERROR=RpcUtil.PARSE_ERROR;
const INVALID_REQUEST=RpcUtil.INVALID_REQUEST;
const METHOD_NOT_FOUND=RpcUtil.METHOD_NOT_FOUND;
const INVALID_PARAMS=RpcUtil.INVALID_PARAMS;
const INTERNAL_ERROR=RpcUtil.INTERNAL_ERROR;

const default_handlers = {
    'web3': require('./web3-rpc-handler'),
    'eth': require('./eth-rpc-handler'),
    'net': require('./net-rpc-handler'),
    'personal': require('./personal-rpc-handler'),
    'miner': require('./miner-rpc-handler'),
    'db': require('./db-rpc-handler')
};


class RPCHandlerApp {
    constructor(esn, handlerMap, trace) {
        this.esn = esn || this;
        this.handlers = handlerMap || default_handlers;
        this.handlers.rpc = this.answerModuleList.bind(this)

        this.defaultBlockNumber = 'latest';
        this.defaultAccount = null;
        this.trace = !!trace;
    }

    validateJSON(jsonString) {
        if(this.trace) console.log('----->', jsonString);
        let request;
        if(typeof jsonString === 'string')
            request = JSON.parse(jsonString); 
        else if(typeof jsonString === 'object')
            request = jsonString;
        else throw new RPCError('expecting either a json string or a javascript object');
        
        if(Array.isArray(request)) return request
        
        if(!request.jsonrpc || request.jsonrpc!=='2.0') throw new RPCError('missing jsonrpc="2.0" in the payload');
        if(request.id===undefined || Number.isNaN(Number.parseInt(request.id)) ) throw new Error('missing a valid id attribute in the payload')
        if(!request.method) throw new Error('missing method attribute in the payload');
        
        request.format=typeof jsonString;
        return request;
    }
    formatResponse(format, req, error, result) {
        let res = new RPCResponse(req, error, result).toFormat(format)
        if(this.trace) console.log('<-----', (typeof res=='string'?res:JSON.stringify(res)));
        return res
    }

    submitAsync(jsonString, callback) {
        if(typeof callback !== 'function') callback=(rpc_response)=>{};
        let request=null;
        let format = typeof jsonString
        let rethrowException = false;
        let self=this
        try {
            request = this.validateJSON(jsonString);
            if(Array.isArray(request)) 
                async.mapSeries(request, 
                    (req,cb)=>{
                        self.submitAsync(req, (response)=>cb(null, response))
                    }, 
                    (e, results)=>{
                        if(format==='string') results=JSON.stringify(results)
                        callback(results, 200)
                })    
            else this.handle_method(request, (error, result)=>{
                setImmediate(()=>{
                    try {
                        if(error) callback( self.formatResponse(format, request, error, null), 200);
                        else callback( self.formatResponse(format, request, null, result), 200);
                    } catch (errorInCallback) {   rethrowException=true; throw errorInCallback }
                })
            });
        } catch (error) {
            if(!rethrowException) callback( self.formatResponse(format, request, error, null), 200)
            else throw error;
        } 
    }
    // SYNCHRONOUS CALL
    submit(jsonString) {
        let request=null;
        let self = this
        try {
            request = this.validateJSON(jsonString);
            let response;
            //It assumes that the callback is called synchronously hence called before the end of the handle_method
            this.handle_method(request, (error, result)=>{
                if(error) response = self.formatResponse(request, error, null);
                else response = self.formatResponse(request, null, result);
            });
            if(response) return response;
            else throw new RPCError('method does not support synchronous call',null,INTERNAL_ERROR);
        } catch (error) {
            return self.formatResponse(request, error, null)
        } 
    }
    handle_method(req, callback) {
        let prefix = req.method.split('_')[0];
        if( typeof this.handlers[prefix] ==='function' )
            // call the handler, under the object context of the given ESN
            this.handlers[prefix].apply(this.esn, [req, callback]);
        else
            callback(new RPCError('unknown method '+req.method, null, METHOD_NOT_FOUND), null);
    }
    answerModuleList(req, callback) {
        if(req.method=='rpc_modules') {
            callback(null, {  
                "eth":"1.0",
                "net":"1.0",
                "personal":"1.0",
                "web3":"1.0",
                "miner":"1.0",
                "db":"1.0"
             })
        } else callback(new RPCError('unknown method '+req.method, null, METHOD_NOT_FOUND), null);
    }
    close(cb) {
        // TODO : to be implemented
        if(!cb) cb = err=>{}
        if(this.esn && this.esn.close) this.esn.close(cb)
        else cb()
    }
}


module.exports = RPCHandlerApp;

/* 
let app=new RPCHandlerApp();
app.submitAsync('{"jsonrpc":"2.0","method":"web3_clientVersion","params":[], "id":"12"}', (out)=>{
    console.log('Output',out)
})

console.log('Sync out:',app.submit('{"jsonrpc":"2.0","method":"web3_clientVersion","params":[], "id":"12"}'))
 */