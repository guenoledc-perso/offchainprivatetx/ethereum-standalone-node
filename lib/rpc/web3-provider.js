const async = require('async')

class ESNWeb3Provider {
    constructor(handler) {
        this.handler = handler // expects to be a JSONRPCHandler
    }
    sendBatch(payloads, callback) {
        if( !Array.isArray(payloads)) throw new TypeError('use sendAsync for non batch payload')
        async.mapSeries(payloads, this.sendAsync.bind(this), (err, results)=>{
            callback(err, results)
        })
    }
    sendAsync(payload, callback) {
        if(Array.isArray(payload)) 
            this.sendBatch(payload,callback)
        else
            this.handler.submitAsync(payload, (response, status) => {
                callback(status==200?null:'Expecting return status 200', response)
            })
    }
    send(payload) {
        return this.handler.submit(payload)
    }
    close(cb) {
        if(!cb) cb = err=>{}
        if(this.handler && typeof this.handler.close === 'function') 
            this.handler.close(cb)
        else cb()
    }
}

module.exports=ESNWeb3Provider