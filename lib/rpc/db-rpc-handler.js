/**
 * Meant to implement https://github.com/ethereum/wiki/wiki/JSON-RPC
 * over the Ethereum Standalone node implemented above EthereumJS modules
 * @Author guenoledc@yahoo.fr
 */

const EthUtils = require('ethereumjs-util');
const RpcUtil = require('./rpc-util');
const RPCError = RpcUtil.RPCError;
const assertParams = RpcUtil.assertParams;


module.exports = function handle_db(req, callback) {
    let params
    switch (req.method.split('_')[1]) {
        case 'putString':
        params = assertParams(req.params, [
            {name:'db', default:'root', format:'UTF8'},
            {name:'name', default:undefined, format:'UTF8'},
            {name:'value', default:undefined, format:'UTF8'},
        ]);
        this.putDb(params.db.toString('utf8'), params.name.toString('utf8'), params.value.toString('utf8'), err=>{
            if(err) callback(err)
            else callback(null, true)
        })
        break

        
        case 'putHex':
        params = assertParams(req.params, [
            {name:'db', default:'root', format:'UTF8'},
            {name:'name', default:undefined, format:'UTF8'},
            {name:'value', default:undefined, format:'Data'},
        ]);
        this.putDb(params.db.toString('utf8'), params.name.toString('utf8'), params.value, err=>{
            if(err) callback(err)
            else callback(null, true)
        })
        break
        
        case 'getString':
        case 'getHex':
        params = assertParams(req.params, [
            {name:'db', default:'root', format:'UTF8'},
            {name:'name', default:undefined, format:'UTF8'},
        ]);
        this.getDb(params.db.toString('utf8'), params.name.toString('utf8'), (err, value)=>{
            if(err) callback(err)
            else callback(null, value)
        })
        break


        default:
            callback(new RPCError('unknown db method '+req.method, null, RpcUtil.METHOD_NOT_FOUND), null);
            break;
    }
}

