/**
 * Meant to implement https://github.com/ethereum/wiki/wiki/JSON-RPC
 * over the Ethereum Standalone node implemented above EthereumJS modules
 * @Author guenoledc@yahoo.fr
 */
const EthUtils = require('ethereumjs-util');
const Transaction = require('ethereumjs-tx');
const BN = EthUtils.BN;
const RpcUtil = require('./rpc-util');
const RPCError = RpcUtil.RPCError;
const assertParams = RpcUtil.assertParams;
const EthFilterEngine = require('../filters')

EthUtils.isValidHash256 = function (hash) {
    return (/^0x[0-9a-fA-F]{64}$/.test(hash)
    );
  };

function assertLogFilterParams(params) {
    params = assertParams(params, [
        {name:'filter', default:undefined, format:'Object'},
    ]);
    let filter = params.filter
    if(filter.address && !Array.isArray(filter.address)) 
        // force address to be an array to avoid multiple cases
        filter.address=[filter.address]  
    // If blockHash is present in the filter criteria, then neither fromBlock nor toBlock are allowed.
    if(filter.blockhash)
        if(filter.fromBlock || filter.toBlock) throw new RPCError('Cannot allow both the blockhash attribute and one of fromBlock or toBlock attribute', null, RpcUtil.INVALID_PARAMS)

    filter = assertParams([filter.fromBlock, filter.toBlock, filter.address, filter.topics, filter.blockhash], [
        {name:'fromBlock', default:'latest', format:'BlockTag'},
        {name:'toBlock', default:'latest', format:'BlockTag'},
        {name:'address', default:null, format:'Array'},
        {name:'topics', default:[], format:'Array'},
        {name:'blockhash', default:null, format:'Data32'}, 
    ])
    if(Array.isArray(filter.address))
        // Check all items are address
        if(filter.address.filter( a=>!EthUtils.isValidAddress(a) ).length>0)
            throw new RPCError('Expecting valid address.es in filter options', null, RpcUtil.INVALID_PARAMS)
    
    return filter
}

module.exports = function handle_eth(req, callback) {
    // initialize the FilterEngine and keep it in the this instance
    // TODO initialize and clear this correctly
    if(!this.filterEngine) this.filterEngine = new EthFilterEngine(0)

    const functions = {
        'protocolVersion': (req, cb) => cb(null, '54'),
        'syncing': (req, cb) => cb(null, false),
        'coinbase': (req, cb) => cb(null, this.coinbase),
        'mining': (req, cb) => cb(null, this.miningActivated),
        'gasPrice': (req, cb) => {
            let infos = this.getBlockchainInfo();
            cb(null, infos.gasPrice);
        },
        'accounts': (req, cb) => {
            let params = assertParams(req.params, []);
    
            try {
                cb(null, this.keystore.listAccounts());
            } catch(error) {
                cb(new RPCError('Fail listing accounts from keystore', error, RpcUtil.INTERNAL_ERROR))
            }
        },
        'blockNumber': (req, cb) => {
            this.getBlock('latest', false, function(error, block){
                cb(error, EthUtils.addHexPrefix(block.number.toString(16)));
            })
        },
        'addressInUse': (req, cb) => { // THIS NOT IN THE STANDARD BUT NEEDED TO SEE IF ADDRESS CAN BE FOUND
            // RETURNS true or false
            let params = assertParams(req.params, [
                {name:'account', default:undefined, format:'Address'},
                {name:'blockNumber', default:'latest', format:'BlockTag'},
            ]);
    
            try {
                this.isAccountExistInDb(params.account, params.blockNumber, (err, exists)=>{
                    if(err) cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR));
                    else cb(null, exists);
                })
            } catch(error) {
                cb(new RPCError('Fail getting account', error, RpcUtil.INVALID_PARAMS))
            }
        },
        'getBalance': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'account', default:undefined, format:'Address'},
                {name:'blockNumber', default:'latest', format:'BlockTag'},
            ]);
    
            try {
                this.getAccount(params.account, false, params.blockNumber, (err, acc)=>{
                    if(err) cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR));
                    else cb(null, EthUtils.addHexPrefix(acc.balance.toString(16)));
                })
            } catch(error) {
                cb(new RPCError('Fail getting account', error, RpcUtil.INVALID_PARAMS))
            }
        },
        'getStorageAt': (req, cb) => { 
            let params = assertParams(req.params, [
                {name:'account', default:undefined, format:'Address'},
                {name:'position', default:undefined, format:'Quantity'},
                {name:'blockNumber', default:'latest', format:'BlockTag'},
            ]);
            try {
                this.getStorageAt(params.account, params.position, params.blockNumber, (err, value) => {
                    if(err) cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR));
                    else cb(null, EthUtils.bufferToHex(new BN(value).toArrayLike(Buffer, 'be', 32)))
                })
            } catch (error) {
                cb(new RPCError('Fail getting storage', error, RpcUtil.INVALID_PARAMS))
            }            
        },
        'getTransactionCount': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'account', default:undefined, format:'Address'},
                {name:'blockNumber', default:'latest', format:'BlockTag'},
            ]);
    
            try {
                this.getAccount(params.account, false, params.blockNumber, (err, acc)=>{
                    if(err) cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR));
                    else cb(null, EthUtils.addHexPrefix(acc.nonce.toString(16)));
                })
            } catch(error) {
                cb(new RPCError('Fail getting account', error, RpcUtil.INVALID_PARAMS))
            }
        },
        'getCode': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'account', default:undefined, format:'Address'},
                {name:'blockNumber', default:'latest', format:'BlockTag'},
            ]);
    
            try {
                this.getAccount(params.account, true, params.blockNumber, (err, acc)=>{
                    if(err) cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR));
                    else cb(null, EthUtils.addHexPrefix(acc.code.toString(16)));
                })
            } catch(error) {
                cb(new RPCError('Fail getting account', error, RpcUtil.INVALID_PARAMS))
            }
        },
        'getBlockByHash': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'hash', default:undefined, format:'Data32'},
                {name:'withTransactions', default:undefined, format:'Bool'},
            ]);
    
            try {
                this.getBlock(params.hash, params.withTransactions, (err, block)=>{
                    if(err) 
                        if(err.toString().indexOf('NotFoundError')>=0) cb(null, null);
                        else cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR));
                    else cb(null, block);
                })
            } catch(error) {
                cb(new RPCError('Fail getting block', error, RpcUtil.INVALID_PARAMS))
            }
        },
        'getBlockByNumber': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'blockNumber', default:'latest', format:'BlockTag'},
                {name:'withTransactions', default:undefined, format:'Bool'},
            ]);
            // TODO : Handle pending cases
            if( params.blockNumber==='pending' ) cb(null, null);
            else try {
                this.getBlock(params.blockNumber, params.withTransactions, (err, block)=>{
                    if(err) 
                        if(err.toString().indexOf('NotFoundError')>=0) cb(null, null);
                        else cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR));
                    else cb(null, block);
                })
            } catch(error) {
                cb(new RPCError('Fail getting block', error, RpcUtil.INVALID_PARAMS))
            }
        },
        'getTransactionByHash': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'hash', default:undefined, format:'Data32'},
            ]);
    
            try {
                this.getTransaction(params.hash, (err, tx)=>{
                    if(err) 
                        if(err.toString().indexOf('NotFoundError')>=0) cb(null, null);
                        else cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR));
                    else cb(null, tx);
                })
            } catch(error) {
                cb(new RPCError('Fail getting transaction by hash', error, RpcUtil.INVALID_PARAMS))
            }
        },
        'getTransactionByBlockHashAndIndex': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'hash', default:undefined, format:'Data32'},
                {name:'index', default:undefined, format:'Quantity'},
            ]);
            try {
                this.getTransactionInBlock(params.hash, params.index, (err, tx)=>{
                    if(err) 
                        if(err.toString().indexOf('NotFoundError')>=0) cb(null, null);
                        else cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR));
                    else cb(null, tx);
                })
            } catch(error) {
                cb(new RPCError('Fail getting transaction by block hash and index', error, RpcUtil.INVALID_PARAMS))
            }
        },
        'getTransactionByBlockNumberAndIndex': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'blockNumber', default:'latest', format:'BlockTag'},
                {name:'index', default:undefined, format:'Quantity'},
            ]);
            if( params.blockNumber==='pending' ) cb(null, null);
            try {
                this.getTransactionInBlock(params.blockNumber, params.index, (err, tx)=>{
                    if(err) 
                        if(err.toString().indexOf('NotFoundError')>=0) cb(null, null);
                        else cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR));
                    else cb(null, tx);
                })
            } catch(error) {
                cb(new RPCError('Fail getting transaction by block number and index', error, RpcUtil.INVALID_PARAMS))
            }
        },
        'getTransactionReceipt': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'hash', default:undefined, format:'Data32'},
            ]);
    
            try {
                this.getTransactionReceipt(params.hash, (err, rcp)=>{
                    if(err) 
                        if(err.toString().indexOf('NotFoundError')>=0) cb(null, null);
                        else cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR));
                    else cb(null, rcp);
                })
            } catch(error) {
                cb(new RPCError('Fail getting transaction by hash', error, RpcUtil.INVALID_PARAMS))
            }
        }, 
        'sign': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'account', default:undefined, format:'Address'},
                {name:'message', default:undefined, format:'UTF8'}
            ]);
    
            try {
                let infos = this.getBlockchainInfo();
                let sign = this.keystore.ecSignMessage(params.message, infos.version, params.account, null);
                cb(null, EthUtils.toRpcSig(sign.v, sign.r, sign.s, infos.version));
            } catch(error) {
                cb(new RPCError('Fail signing message', error, RpcUtil.INVALID_PARAMS))
            }
        },
        'sendRawTransaction': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'raw', default:undefined, format:'Data'},
            ]);
            
            try {
                let tx = new Transaction(params.raw)
                this.addTransaction(tx, cb) 
            } catch(error) {
                cb(new RPCError('Fail submitting raw transaction', error, RpcUtil.INTERNAL_ERROR))
            }

        },
        'sendTransaction': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'tx', default:undefined, format:'Object'},
            ]);
            let tx=params.tx;
            tx = assertParams([tx.from, tx.to, tx.gasLimit||tx.gas, tx.gasPrice, tx.value, tx.data, tx.nonce],[
                {name:'from', default:undefined, format:'Address'},
                {name:'to', default:null, format:'Address'},
                {name:'gasLimit', default:90000, format:'Quantity'},
                {name:'gasPrice', default:0, format:'Quantity'},
                {name:'value', default:null, format:'Quantity'},
                {name:'data', default:null, format:'Data'},
                {name:'nonce', default:null, format:'Quantity'},
            ]);
            if( tx.to===null) delete tx.to // this is when creating a contract
            if( tx.value===null ) delete tx.value
            if( tx.data===null) delete tx.data
            if( tx.nonce===null ) delete tx.nonce
            try {
                let from = tx.from; 
                RpcUtil.prepareNewTransaction(tx, 'latest', this, (err, tx) => { // TODO: this does not set the nonce correctly in case of multiple transaction in sequence
                    if(!err) try {
                        tx = this.keystore.signTransaction(from, tx)
                        this.addTransaction(tx, cb) 
                        return
                    } catch(failSigningAndAdd) {err=failSigningAndAdd}               
                    if(err) cb( new RPCError('Fail signing and submitting transaction: '+(err.message?err.message:''), err, RpcUtil.INTERNAL_ERROR), null);
                    else cb(new RPCError('Unexpected situation in submitting transaction:', null, RpcUtil.INTERNAL_ERROR))
                    })
            } catch(error) {
                cb(new RPCError('Fail signing and submitting transaction', error, RpcUtil.INTERNAL_ERROR))
            }

        },
        'call': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'tx', default:undefined, format:'Object'},
                {name:'blockNumber', default:'latest', format:'BlockTag'},
            ]);
            let tx=params.tx;
            tx = assertParams([tx.from, tx.to, tx.gas, tx.gasPrice, tx.value, tx.data, tx.nonce],[
                {name:'from', default:null, format:'Address'},
                {name:'to', default:null, format:'Address'},
                {name:'gasLimit', default:null, format:'Quantity'},
                {name:'gasPrice', default:null, format:'Quantity'},
                {name:'value', default:null, format:'Quantity'},
                {name:'data', default:null, format:'Data'},
            ]);
            // remove not provided optional params
            if( tx.to===null) delete tx.to
            if( tx.gasPrice===null) delete tx.gasPrice
            if( tx.value===null ) delete tx.value
            if( tx.data===null) delete tx.data
            try {
                let from = tx.from; 
                delete tx.from;
                tx = new Transaction(tx)
                tx._from = from
                //if(from && !this.keystore.isLocked(from)) tx = this.keystore.signTransaction(from, tx)
                // if from is not provided (optional in spec), use an anonymous account
                if(!tx._from) tx._from = this.keystore.signTransactionWithAnonymousAccount(tx)
                this.playTransaction(tx, params.blockNumber, (err, receipt) => {
                    if(err) cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR), null)
                    else if(receipt.status==0) cb(new RPCError(receipt.exception, null, RpcUtil.INTERNAL_ERROR))
                    else cb(null, receipt.return)
                })               
            } catch(error) {
                cb(new RPCError('Fail in eth_call transaction', error, RpcUtil.INTERNAL_ERROR))
            }
        },
        'estimateGas': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'tx', default:undefined, format:'Object'},
                {name:'blockNumber', default:'latest', format:'BlockTag'},
            ]);
            let tx=params.tx;
            tx = assertParams([tx.from, tx.to, tx.gas, tx.gasPrice, tx.value, tx.data, tx.nonce],[
                {name:'from', default:null, format:'Address'},
                {name:'to', default:null, format:'Address'},
                {name:'gasLimit', default:null, format:'Quantity'},
                {name:'gasPrice', default:null, format:'Quantity'},
                {name:'value', default:null, format:'Quantity'},
                {name:'data', default:null, format:'Data'},
            ]);
            // remove not provided optional params
            if( tx.to===null) delete tx.to // this is when creating a contract
            if( tx.gasPrice===null) delete tx.gasPrice
            if( tx.value===null ) delete tx.value
            if( tx.data===null) delete tx.data
            try {
                let from = tx.from; 
                delete tx.from;
                tx = new Transaction(tx)
                tx._from = from
                //if(from && !this.keystore.isLocked(from)) tx = this.keystore.signTransaction(from, tx)
                // if from is not provided (optional in spec), use an anonymous account
                if(!tx._from) tx._from = this.keystore.signTransactionWithAnonymousAccount(tx)
                this.playTransaction(tx, params.blockNumber, (err, receipt) => {
                    if(err) cb(new RPCError(err, null, RpcUtil.INTERNAL_ERROR), null)
                    else cb(null, receipt.gasUsed)
                })               
            } catch(error) {
                cb(new RPCError('Fail in eth_estimateGas transaction', error, RpcUtil.INTERNAL_ERROR))
            }
        },
        'newBlockFilter': (req, cb) => {
            try {
                let ret = this.filterEngine.newFilter(this, EthFilterEngine.BLOCK_TYPE)
                ret = this.filterEngine.add(ret)
                cb(null, ret)
            } catch (failCreatingFilter) {
                cb(new RPCError('Fail creating block filter', failCreatingFilter, RpcUtil.INTERNAL_ERROR))
            }
        },
        'uninstallFilter': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'id', default:undefined, format:'Quantity'},
            ]);
            try {
                let ok = this.filterEngine.del(EthUtils.bufferToHex(params.id))
                cb(null, ok)
            } catch (failRemovingFilter) {
                cb(new RPCError('Fail removing filter', failRemovingFilter, RpcUtil.INTERNAL_ERROR))
            }
        },
        'newFilter': (req, cb) => {
            let filter = assertLogFilterParams(req.params)
            try {
                let ret = this.filterEngine.newFilter(this, EthFilterEngine.LOG_TYPE, filter, true)
                ret = this.filterEngine.add(ret)
                cb(null, ret)
            } catch (failCreatingFilter) {
                cb(new RPCError('Fail creating log filter', failCreatingFilter, RpcUtil.INTERNAL_ERROR))
            }
        },
        'getLogs': (req, cb) => {
            let filter = assertLogFilterParams(req.params)
            try {
                this.filterEngine.getAllWithOptions(this, filter, cb)
            } catch (failCreatingFilter) {
                cb(new RPCError('Fail getting logs', failCreatingFilter, RpcUtil.INTERNAL_ERROR))
            }
        },
        'getFilterChanges': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'id', default:undefined, format:'Quantity'},
            ]);
            try {
                let ret = this.filterEngine.get(EthUtils.bufferToHex(params.id))
                setImmediate( () => cb(null, ret) )
            } catch (failReadingFilter) {
                cb(new RPCError('Fail executing FilterChanges', failReadingFilter, RpcUtil.INTERNAL_ERROR))
            }
        },
        'getFilterLogs': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'id', default:undefined, format:'Quantity'},
            ]);
            try {
                this.filterEngine.getAll(EthUtils.bufferToHex(params.id), cb)
                //setImmediate( () => cb(null, ret) )
            } catch (failReadingFilter) {
                cb(new RPCError('Fail executing FilterLogs', failReadingFilter, RpcUtil.INTERNAL_ERROR))
            }
        },

    }


    let method = req.method.split('_')[1];
    
    if(functions[method]) functions[method].apply(this, [req, callback]);
    else callback(new RPCError('unknown eth method '+req.method, null, RpcUtil.METHOD_NOT_FOUND), null);
    
        
}
