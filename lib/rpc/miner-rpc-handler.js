/**
 * Meant to implement https://github.com/ethereum/wiki/wiki/JSON-RPC
 * over the Ethereum Standalone node implemented above EthereumJS modules
 * @Author guenoledc@yahoo.fr
 */
const EthUtils = require('ethereumjs-util');
const Transaction = require('ethereumjs-tx');

const BN = EthUtils.BN;
const RpcUtil = require('./rpc-util');
const RPCError = RpcUtil.RPCError;
const assertParams = RpcUtil.assertParams;

EthUtils.isValidHash256 = function (hash) {
    return (/^0x[0-9a-fA-F]{64}$/.test(hash)
    );
  };



module.exports = function handle_miner(req, callback) {

    const functions = {
        'setExtra': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'extra', default:undefined, format:'Data'},
            ]);
            this.blockchain.extra_data = EthUtils.bufferToHex( EthUtils.toBuffer(params.extra).slice(0,32))
            callback(null, this.blockchain.extra_data)
        },
        'setGasPrice': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'minGasPrice', default:undefined, format:'Quantity'},
            ]);
            this.blockchain.minGasPrice = params.minGasPrice
            callback(null, this.blockchain.minGasPrice.toString())
        },
        'setEtherbase': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'coinbase', default:undefined, format:'Address'},
            ]);
            this.coinbase = params.coinbase
            callback(null, this.coinbase)
        },
        'start': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'threads', default:0, format:'Quantity'},
            ]);
            this.miningActivated = true
            this.triggerBlockCreation()
            callback(null, this.miningActivated)
        },
        'stop': (req, cb) => {
            let params = assertParams(req.params, []);
            this.miningActivated = false
            callback(null, this.miningActivated)
        },

    }


    let method = req.method.split('_')[1];
    
    if(functions[method]) functions[method].apply(this, [req, callback]);
    else callback(new RPCError('unknown miner method '+req.method, null, RpcUtil.METHOD_NOT_FOUND), null);
    
        
}
