/**
 * Meant to implement https://github.com/ethereum/wiki/wiki/JSON-RPC
 * over the Ethereum Standalone node implemented above EthereumJS modules
 * @Author guenoledc@yahoo.fr
 */
const EthUtils = require('ethereumjs-util');
const Transaction = require('ethereumjs-tx');

const BN = EthUtils.BN;
const RpcUtil = require('./rpc-util');
const RPCError = RpcUtil.RPCError;
const assertParams = RpcUtil.assertParams;

EthUtils.isValidHash256 = function (hash) {
    return (/^0x[0-9a-fA-F]{64}$/.test(hash)
    );
  };



module.exports = function handle_personal(req, callback) {

    const functions = {
        'importRawKey': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'privateKey', default:undefined, format:'UTF8'},
                {name:'passphrase', default:undefined, format:'UTF8'}
            ]);
    
            try {
                let address = this.keystore.newAccountFromPrivateKey(params.privateKey, params.passphrase);
                cb(null, address);
            } catch(error) {
                cb(new RPCError('Fail importing private key', error, RpcUtil.INTERNAL_ERROR))
            }

        },
        'listAccounts': (req, cb) => {
            let params = assertParams(req.params, []);
    
            try {
                cb(null, this.keystore.listAccounts());
            } catch(error) {
                cb(new RPCError('Fail listing accounts from keystore', error, RpcUtil.INTERNAL_ERROR))
            }
        },
        'lockAccount': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'account', default:undefined, format:'Address'},
            ]);
    
            try {
                this.keystore.lockAccount(params.account);
                cb(null, '');
            } catch(error) {
                cb(new RPCError('Fail locking the account', error, RpcUtil.INTERNAL_ERROR))
            }
        },
        'newAccount': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'passphrase', default:undefined, format:'UTF8'},
            ]);
    
            try {
                cb(null, this.keystore.newAccount(params.passphrase));
            } catch(error) {
                cb(new RPCError('Fail creating new account', error, RpcUtil.INTERNAL_ERROR))
            }
        },
        'unlockAccount': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'account', default:undefined, format:'Address'},
                {name:'passphrase', default:undefined, format:'UTF8'},
                {name:'duration', default:300, format:'Quantity'}
            ]);
            
            try {
                this.keystore.unlockAccount(params.account, params.passphrase.toString('utf8'), params.duration.toNumber());
                cb(null, true);
            } catch(error) {
                cb(new RPCError('Fail unlocking the account', error, RpcUtil.INTERNAL_ERROR))
            }
        },
        'sign': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'message', default:undefined, format:'UTF8'},
                {name:'account', default:undefined, format:'Address'},
                {name:'passphrase', default:undefined, format:'UTF8'},
            ]);
    
            try {
                let infos = this.getBlockchainInfo();
                let sign = this.keystore.ecSignMessage(params.message, infos.version, params.account, params.passphrase.toString('utf8'));
                cb(null, EthUtils.toRpcSig(sign.v, sign.r, sign.s, infos.version));
            } catch(error) {
                cb(new RPCError('Fail signing message', error, RpcUtil.INVALID_PARAMS))
            }

        },
        'ecRecover': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'message', default:undefined, format:'UTF8'},
                {name:'signature', default:undefined, format:'UTF8'},
            ]);
    
            try {
                let infos = this.getBlockchainInfo();
                let sign = this.keystore.fromRpcSig(params.signature)
                let pubkey = this.keystore.ecRecover(params.message, sign, infos.version);
                cb(null, EthUtils.bufferToHex( EthUtils.pubToAddress(pubkey) ));
            } catch(error) {
                cb(new RPCError('Fail recovering public key', error, RpcUtil.INVALID_PARAMS))
            }

        },
        'sendTransaction': (req, cb) => {
            let params = assertParams(req.params, [
                {name:'tx', default:undefined, format:'Object'},
                {name:'passphrase', default:undefined, format:'UTF8'},
            ]);
            let tx=params.tx;
            tx = assertParams([tx.from, tx.to, tx.gasLimit||tx.gas, tx.gasPrice, tx.value, tx.data, tx.nonce],[
                {name:'from', default:undefined, format:'Address'},
                {name:'to', default:null, format:'Address'},
                {name:'gasLimit', default:90000, format:'Quantity'},
                {name:'gasPrice', default:0, format:'Quantity'},
                {name:'value', default:null, format:'Quantity'},
                {name:'data', default:null, format:'Data'},
                {name:'nonce', default:null, format:'Quantity'},
            ]);
            if( tx.to===null ) delete tx.to
            if( tx.value===null ) delete tx.value
            if( tx.data===null) delete tx.data
            if( tx.nonce===null ) delete tx.nonce
            try {
                let from = tx.from; 
                RpcUtil.prepareNewTransaction(tx, 'latest', this, (err, tx) => { // TODO: this does not set the nonce correctly in case of multiple transaction in sequence
                    if(!err) try {
                        tx = this.keystore.signTransaction(from, tx, params.passphrase.toString('utf8'))
                        this.addTransaction(tx, cb) 
                        return
                    } catch(failSigningAndAdd) {err=failSigningAndAdd}               
                    if(err) cb( new RPCError('Fail signing and submitting transaction: '+(err.message?err.message:''), err, RpcUtil.INTERNAL_ERROR), null);
                    else cb(new RPCError('Unexpected situation in submitting transaction:', null, RpcUtil.INTERNAL_ERROR))
                    })
            } catch(error) {
                cb(new RPCError('Fail signing and submitting transaction', error, RpcUtil.INTERNAL_ERROR))
            }

        },
        'signTransaction': (req, cb) => { // receives the same parameters as sendTransaction but returned the serialized signed transaction that can be passed to the sendRawTransaction
            let params = assertParams(req.params, [
                {name:'tx', default:undefined, format:'Object'},
                {name:'passphrase', default:null, format:'UTF8'},
            ]);
            let tx=params.tx;
            tx = assertParams([tx.from, tx.to, tx.gasLimit||tx.gas, tx.gasPrice, tx.value, tx.data, tx.nonce],[
                {name:'from', default:undefined, format:'Address'},
                {name:'to', default:null, format:'Address'},
                {name:'gasLimit', default:90000, format:'Quantity'},
                {name:'gasPrice', default:0, format:'Quantity'},
                {name:'value', default:null, format:'Quantity'},
                {name:'data', default:null, format:'Data'},
                {name:'nonce', default:null, format:'Quantity'},
            ]);
            if( tx.to===null ) delete tx.to
            if( tx.value===null ) delete tx.value
            if( tx.data===null) delete tx.data
            if( tx.nonce===null ) delete tx.nonce
            try {
                let from = tx.from; 
                RpcUtil.prepareNewTransaction(tx, 'latest', this, (err, tx) => { // TODO: this does not set the nonce correctly in case of multiple transaction in sequence
                    if(!err) try {
                        // adding the current chain version or id needed for the signature
                        chainId = this.getBlockchainInfo().version
                        chainId = '0x'+new BN(EthUtils.stripHexPrefix(chainId)).toString('hex')
                        tx.chainId = chainId
                        tx = this.keystore.signTransaction(from, tx, params.passphrase?params.passphrase.toString('utf8'):null)
                        tx = '0x'+tx.serialize().toString('hex')
                    } catch(failSigningAndAdd) {err=failSigningAndAdd}               
                    if(err) cb( new RPCError('Fail signing and submitting transaction: '+(err.message?err.message:''), err, RpcUtil.INTERNAL_ERROR), null);
                    else cb(null, tx)
                    })
            } catch(error) {
                cb(new RPCError('Fail signing and submitting transaction', error, RpcUtil.INTERNAL_ERROR))
            }

        },
    }


    let method = req.method.split('_')[1];
    
    if(functions[method]) functions[method].apply(this, [req, callback]);
    else callback(new RPCError('unknown personal method '+req.method, null, RpcUtil.METHOD_NOT_FOUND), null);
    
        
}
