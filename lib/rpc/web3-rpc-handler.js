/**
 * Meant to implement https://github.com/ethereum/wiki/wiki/JSON-RPC
 * over the Ethereum Standalone node implemented above EthereumJS modules
 * @Author guenoledc@yahoo.fr
 */

const EthUtils = require('ethereumjs-util');
const RpcUtil = require('./rpc-util');
const RPCError = RpcUtil.RPCError;

module.exports = function handle_web3(req, callback) {
    switch (req.method.split('_')[1]) {
        case 'clientVersion':
            let infos = this.getBlockchainInfo();
            callback(null, 'Ethereum-Standalone-Node/v0.0.1/'+infos.version);
            break;
        case 'sha3':
            if(!req.params || !req.params[0] || typeof req.params[0] !== 'string')
                callback(new RPCError('invalid parameter', null, RpcUtil.INVALID_PARAMS))
            else callback(null,EthUtils.bufferToHex(EthUtils.keccak256(req.params[0])));
            break;
        default:
            callback(new RPCError('unknown web3 method '+req.method, null, RpcUtil.METHOD_NOT_FOUND), null);
            break;
    }
}

