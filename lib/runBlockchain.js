'use strict';

const async = require('async');
const EthUtils = require('ethereumjs-util');

/**
 * Processes blocks and adds them to the blockchain
 * @method vm.runBlockchain
 * @param {Blockchain} blockchain A [blockchain](https://github.com/ethereum/ethereumjs-blockchain) that to process
 * @param {Function} cb the callback function that returns 
 *  an array (one per block) of array (one per transaction) object {transaction: {}, receipt: {}, result: {}}
 */
module.exports = function (blockchain, cb) {
  var self = this;
  var headBlock, parentState;
  let arrayOfBlockResults = [];

  // parse arguments
  if (typeof blockchain === 'function') {
    cb = blockchain;
    blockchain = undefined;
  }

  blockchain = blockchain || self.blockchain;

  function cleanResultTx(tx) {
    tx._hash = tx.hash();
    tx.hash = () => tx._hash;
    //delete tx.raw;
    //delete tx.serialize;
    //delete tx._fields;
    //delete tx.toJSON;
    return tx;
  }
  function cleanResultResult(result) {
    result.return = result.vm.return;
    result.exception = result.vm.exceptionError;
    delete result.vm;
    return result;
  }
  function cleanResultReceipt(receipt) {
    // transform logs into {address, data, topics} like in https://github.com/ethereum/wiki/wiki/JSON-RPC#returns-31
    let logs = receipt.logs.map( (log) => { 
      return {
        address: EthUtils.bufferToHex(log[0]),
        data: EthUtils.bufferToHex(log[2]),
        topics: log[1].map( topic=> EthUtils.bufferToHex(topic) )
      } 
    });
    receipt.rawLogs = receipt.logs;
    receipt.logs = logs;
    return receipt;
  }
  function cleanResultBlock(block) {
    return block;
  }
  //console.log('VM iterator heads', blockchain._heads['vm'])
  // setup blockchain iterator
  blockchain.iterator('vm', processBlock, (err) => {
    // reconstruct the array of array of result
    let newArray = arrayOfBlockResults.map( 
      (blockResult) => blockResult.transactions.map( 
        (tx, txIndex) => ({transaction: cleanResultTx(tx), 
                          receipt: cleanResultReceipt(blockResult.receipts[txIndex]), 
                          result: cleanResultResult(blockResult.results[txIndex]),
                          block: cleanResultBlock(blockResult.block),
                          index: txIndex})
        ) 
      );
    cb(err, newArray);
  });

  function processBlock(block, reorg, cb) {
    //console.log("ProcessingBlock", block.header.number, block.hash().toString('hex'))
    async.series([getStartingState, runBlock], cb);

    // determine starting state for block run
    function getStartingState(cb) {
      // if we are just starting or if a chain re-org has happened
      if (!headBlock || reorg) {
        blockchain.getBlock(block.header.parentHash, function (err, parentBlock) {
          headBlock = parentBlock; // MODIFICATION TO ALLOW STARTING FROM AN EXISTING DATABASE
          parentState = parentBlock.header.stateRoot;
          // generate genesis state if we are at the genesis block
          // we don't have the genesis state
          // MODIFICATION TO ALLOW STARTING FROM AN EXISTING DATABASE (headBlock was never pre initialized)
          if (!headBlock || headBlock.header.number.length==0) {
            return self.stateManager.generateCanonicalGenesis(cb);
          } else {
            cb(err);
          }
        });
      } else {
        parentState = headBlock.header.stateRoot;
        cb();
      }
    }

    // run block, update head if valid
    function runBlock(cb) {
      self.runBlock({
        block: block,
        root: parentState,
        generate: true ///// MODIFICATION TO ENABLE STATEROOT ADJUSTMENT AFTER COMMIT
      }, function (err, results) {
        if( results ) {
          //console.log("RunBlockCallback:", results);
          results.transactions = block.transactions;
          results.block = block;
          arrayOfBlockResults.push(results);
        }
        if (err) {
          // remove invalid block
          blockchain.delBlock(block.header.hash(), function () {
            cb(err, null);
          });
        } else {
          // set as new head block
          headBlock = block;
          cb(null, results);
        }
      });
    }
  }
};