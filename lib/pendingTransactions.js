const async = require('async')
const StateManager = require('ethereumjs-vm/dist/stateManager')
const Transaction = require('ethereumjs-tx')
const EthUtils = require('ethereumjs-util')
const BN = EthUtils.BN
const rlp = EthUtils.rlp


function loopMapOfAccountNonceTx(mapOfAccountNonceTx, onTx) {
    if(typeof onTx !== 'function') return
    if(!mapOfAccountNonceTx) return
    Object.entries(mapOfAccountNonceTx).forEach( ([acc, nonceObj]) => {
        Object.entries(nonceObj).forEach( ([nonce, tx]) => {
            try{ onTx(acc, nonce, tx) } catch(ignoreError) {}
        })
    });
}
function formatPendingTransaction(tx) {
    // create a response respecting the format in https://github.com/ethereum/wiki/wiki/JSON-RPC#returns-28
    let transaction = {
        blockHash: null,
        blockNumber: null,
        from: EthUtils.bufferToHex(tx.from),
        gas: EthUtils.bufferToHex(tx.gas),
        gasPrice: '0x'+new BN(tx.gasPrice).toString(16),
        hash: EthUtils.bufferToHex(tx.hash()),
        input: EthUtils.bufferToHex(tx.input),
        nonce: '0x'+new BN(tx.nonce).toString(16),
        to: '0x'+new BN(tx.to).toString(16),
        transactionIndex: null,
        value: '0x'+new BN(tx.value).toString(16),
        v: '0x'+new BN(tx.v).toString(16),
        r: EthUtils.bufferToHex(tx.r),
        s: EthUtils.bufferToHex(tx.s)
    };

    return transaction;
}

class PendingTransactions {
    constructor(stateManager, vmAcceptsNonceGaps) {
        if(!stateManager) throw new TypeError('Expect a valid StateManager to initialize the PendingTransaction class')
        this.stateManager=stateManager
        this.vmAcceptsNonceGaps = !!vmAcceptsNonceGaps // NOTE Ordering

        // transactions ready to be processed map of account->nonce->tx
        this.pending = {}
        // transactions waiting to reach the appropriate nonce map of account->nonce->tx
        this.queued = {}
        // map by account->max(nonce)
        this.higherPendingNonce = {}
        // array of currently processing transactions by hash
        this.processing = {}
    }
    _pushInQueued(tx) {
        let acc = EthUtils.bufferToHex(tx.from) // Buffer
        let nonceStr = new BN(tx.nonce).toString(10,32) // zero padding 32 bytes nonce
        if(!this.queued[acc]) this.queued[acc] = {}
        this.queued[acc][nonceStr] = tx
    }
    _pushInPending(tx) {
        let acc = EthUtils.bufferToHex(tx.from) // Buffer
        let nonce = new BN(tx.nonce) // BN
        let nonceStr = nonce.toString(10,32) // zero padding 32 bytes nonce
        if(!this.pending[acc]) this.pending[acc] = {}
        this.pending[acc][nonceStr] = tx
        this._updateHigherPendingNonce(acc, nonce)
    }
    _updateHigherPendingNonce(acc,nonce) {
        if(!this.higherPendingNonce[acc]) this.higherPendingNonce[acc]=nonce
        else if( this.higherPendingNonce[acc].lt(nonce) ) this.higherPendingNonce[acc]=nonce
    }
    _reAssessQueued(acc, cb) {
        let queued = this.queued[acc]
        delete this.queued[acc] // remove as if there was never a queued item
        if(!queued) return cb()
        let self = this
        async.eachSeries( Object.keys(queued).sort(), (nonce, cb)=>{
            self.push(queued[nonce], cb)
        }, err=>{
            cb(err)
        } )
    }
    _checkTx(tx, cb) { 
        // NOTE Ordering: For using the ethereum standalone node with a privacy node, the nonce sequence cannot be guaranteed
        //      because not all privacy nodes receive all account's transactions. Hence, a node must accept a nonce that 
        //      is beyond the currently expected nonce without waiting (but only in privacy node) and it must set the 
        //      account to skip the missing nonces so normal controls can apply.
        //      So, in that case of utilisation, the account nonce must be set to the tx nonce before reaching this point

        // checks the conditions for a tx to be processed, queued or rejected
        // callback cb will be called with (rejectReason or null, status in ['queued', 'pending'])
        if(!tx) return cb('Improper call of _checkTX', null)
        let acc = tx.from // Buffer
        let accHex = EthUtils.bufferToHex(acc)
        let nonce = new BN(tx.nonce) // BN for sorting
        let gasLimit = new BN(tx.gasLimit) 
        let currentNonce = null
        let currentBalance = null
        let self = this

        async.seq(
            cb=>{ // get the current state nonce and the max nonce in pending
                this.stateManager.getAccount(acc, (err, account) => {
                    if(err) cb(err)
                    currentNonce = new BN(account.nonce)
                    this._updateHigherPendingNonce(accHex, currentNonce.addn(-1)) // init to the nonce before current if new in cache
                    currentNonce = this.higherPendingNonce[accHex]
                    currentBalance = new BN(account.balance)
                    cb()
                })
            },
            cb=>{ // is nonce after the max pending nonce (or the current nonce)
                // nonce > max+1
                if(!self.vmAcceptsNonceGaps) // NOTE Ordering
                    if( nonce.gt(this.higherPendingNonce[accHex].addn(1).abs()) ) // BN<-1> + 1 is BN<-0> that is lt BN<0> !!! Hence I need an abs
                        return cb('queued') // force going to the end of the sequence
                cb()
            },
            cb=>{ // check if nonce is before the current nonce
                if( currentNonce.gt(nonce) )
                    return cb('the tx doesn\'t have the correct nonce. account has nonce of ' + currentNonce.toString() + '; tx has nonce of ' + nonce.toString())
                cb()
            },
            cb=>{ // check balance
                if( currentBalance.lt(tx.getUpfrontCost()) ) 
                    return cb('sender doesn\'t have enough funds to send tx. The upfront cost is: ' + tx.getUpfrontCost().toString() + ' and the sender\'s account only has: ' + currentBalance.toString())
                if( gasLimit.lt(tx.getBaseFee()) )
                    return cb('base fee exceeds gas limit '+gasLimit.toString()+'<'+tx.getBaseFee().toString())
                cb()
            },
        )( err=>{
            //console.log('CheckTx:', EthUtils.bufferToHex(tx.hash()), accHex, nonce.toString(), err)
            if(err==='queued') { // this is not really an error, queued
                cb(null, 'queued' )
            } else if(err) { // this is a rejection error
                return cb(err, null)
            } else { // pending
                cb(null, 'pending' )
            }
        } )
    }
    push(tx, cb) {
        this._checkTx( tx, (err, status) =>{
            this._delFromProcessing(tx) // remove it from processing if it where there
            console.log('PUSH PENDING TRANSACTION', status,  EthUtils.bufferToHex(tx.hash()), EthUtils.bufferToHex(tx.from), EthUtils.bufferToHex(tx.nonce))
            
            if(err) cb(err, null)
            else if(status==='queued') { // this is not really an error, add tx in queued
                this._pushInQueued(tx)
                cb(null, EthUtils.bufferToHex(tx.hash()) )
            } else if(status==='pending') { // add the tx in pending
                this._pushInPending(tx)
                this._reAssessQueued(EthUtils.bufferToHex(tx.from) , err=>{
                    if(err) console.error('Error when reassesing the queued transaction of '+EthUtils.bufferToHex(tx.hash()), err)
                    cb(null, EthUtils.bufferToHex(tx.hash()) )
                })
            } else cb('unexpected status from _checkTx:'+status, null)
            
            console.log('PUSH PENDING TRANSACTIONS INSPECTION\n', this.inspect())
        } )
    }
    firstPending(deleteIt) { // the first pending tx by priority order: nonce, account 
        // first make an array of keys to be sorted
        let keys = []
        loopMapOfAccountNonceTx(this.pending, (acc, nonce, tx)=>{
            keys.push(nonce+':'+acc)
        })
        if(keys.length==0) return null;
        keys=keys.sort()
        let nonce, acc, tx
        [nonce, acc] = keys.shift().split(':')
        tx=this.pending[acc][nonce]
        if( deleteIt===true ) delete this.pending[acc][nonce]
        if( Object.keys(this.pending[acc]).length == 0 ) delete this.pending[acc]
        // if it has been removed from the pending, add it to the processing
        if( deleteIt===true ) this._addForProcessing(tx)
        return tx
    }
    popPending(cb) { // gives to the callback(err, tx) the first pending and check it is still valid
        // first make an array of keys to be sorted
        let tx=this.firstPending(true) // and adds it into the processing list

        if(!tx) return cb(null, null)
        let self = this
        
        this._checkTx( tx, (err, status) => {
            if(err) {
                console.error('The transaction is not valid. Cannot be processed:', EthUtils.bufferToHex(tx.hash()), err)
                self.popPending(cb) // try the next one
            } else if(status==='queued') {
                console.log('This should really never happen. Pushing it back to the structure')
                self.push(tx, (err, hash)=> self.popPending(cb)) // then try the next one
            } else if(status==='pending') {
                // set the max nonce of that account to the tx nonce to handle a race condition
                this._updateHigherPendingNonce(EthUtils.bufferToHex(tx.from), new BN(tx.nonce).addn(1)) 

                console.log('POP PENDING TRANSACTION', EthUtils.bufferToHex(tx.hash()), EthUtils.bufferToHex(tx.from), EthUtils.bufferToHex(tx.nonce))
                console.log('POP PENDING TRANSACTIONS INSPECTION\n', this.inspect())
                cb(null,tx)
            }
        }) 
    }
    stateChanged(cb) { // when user knows that the state has changed, it may ask to recheck the transactions, pending and queued to put them in the right category
        let self = this
        async.eachSeries( Object.keys(this.queued),
            (acc, cb) => self._reAssessQueued(acc, cb),
        cb)
    }
    getLastKnownNonce(address) {
        if( address in this.higherPendingNonce ) return this.higherPendingNonce[address]
        else return null
    }
    _addForProcessing(tx) {
        this.processing[EthUtils.bufferToHex(tx.hash())] = tx
    }
    _delFromProcessing(tx) {
        delete this.processing[EthUtils.bufferToHex(tx.hash())]
    }
    getAllProcessing() {
        return Object.values(this.processing)
    }
    clearAllProcessing() {
        this.processing = {}
    }


    inspect() {
        let result = {
            pending: {},
            queued:{},
            processing:[],
            maxNonce:{}
        }
        let map = result.pending
        loopMapOfAccountNonceTx(this.pending, (acc, nonce, tx) => {
            if(!map[acc]) map[acc]={}
            map[acc][nonce] = EthUtils.bufferToHex(tx.hash())+': '+ new BN(tx.value).toString()+' wei + '+new BN(tx.gasLimit).toString()+' gas x '+new BN(tx.gasPrice).toString()+' wei'
        })
        map = result.queued
        loopMapOfAccountNonceTx(this.queued, (acc, nonce, tx) => {
            if(!map[acc]) map[acc]={}
            map[acc][nonce] = EthUtils.bufferToHex(tx.hash())+': '+ new BN(tx.value).toString()+' wei + '+new BN(tx.gasLimit).toString()+' gas x '+new BN(tx.gasPrice).toString()+' wei'
        })
        Object.entries(this.higherPendingNonce).forEach( ([acc, nonce])=>{
            result.maxNonce[acc]=nonce.toString()
        })
        result.processing = this.getAllProcessing().map(tx=>EthUtils.bufferToHex(tx.hash()))
        return result
    }
    toJSON() {
        let result = {
            pending: {},
            queued:{},
        }
        let map = result.pending
        loopMapOfAccountNonceTx(this.pending, (acc, nonce, tx) => {
            if(!map[acc]) map[acc]={}
            nonce = new BN(nonce).toString()
            map[acc][nonce] = formatPendingTransaction(tx)
        })
        map = result.queued
        loopMapOfAccountNonceTx(this.queued, (acc, nonce, tx) => {
            if(!map[acc]) map[acc]={}
            nonce = new BN(nonce).toString()
            map[acc][nonce] = formatPendingTransaction(tx)
        })
        return result
    }
    serialize() {
        let result = []
        
        loopMapOfAccountNonceTx(this.pending, (acc, nonce, tx) => {
            result.push(tx.serialize())
        })
        
        loopMapOfAccountNonceTx(this.queued, (acc, nonce, tx) => {
            result.push(tx.serialize())
        })
        return rlp.encode(result)
    }
    // rlp decode as an array of transactions and push them one by one to fall in the rights categories
    initFromData(data, cb) {
        data = EthUtils.toBuffer(data)
        let transactions = rlp.decode(data)
        let self = this
        let results = []
        async.eachSeries(transactions, (txData, cb)=>{
            self.push(new Transaction(txData), (err, hash) =>{
                if(err) results.push(err)
                else results.push(hash)
                cb()
            })
        }, err=>{
            cb(null, results)
        })
    }
    exists(hash) {
        let result = null
        loopMapOfAccountNonceTx(this.pending, (acc, nonce, tx) => {
            if(result) return
            if( hash==EthUtils.bufferToHex(tx.hash()) ) result='pending'
        })
        if(result) return result
        loopMapOfAccountNonceTx(this.queued, (acc, nonce, tx) => {
            if(result) return
            if( hash==EthUtils.bufferToHex(tx.hash()) ) result='queued'
        })
        if(result) return result
        if( hash in this.processing ) return 'processing'
        return false
    }
    getByHash(hash) {
        let result = null
        loopMapOfAccountNonceTx(this.pending, (acc, nonce, tx) => {
            if(result) return
            if( hash==EthUtils.bufferToHex(tx.hash()) ) result=tx
        })
        if(result) return result
        loopMapOfAccountNonceTx(this.queued, (acc, nonce, tx) => {
            if(result) return
            if( hash==EthUtils.bufferToHex(tx.hash()) ) result=tx
        })
        if(result) return result
        if( hash in this.processing ) return this.processing[hash]
        return null
    }
}

PendingTransactions.formatPendingTransaction = formatPendingTransaction
module.exports = PendingTransactions