# ethereum-standalone-node

Manages an ethereum blockchain database with an in memory EVM

## Compatibility
At the time of this build, the VM is compatible with solidity compiler up to version 0.5.1 (not tested all versions, but it failed with version 0.5.5)

## Other